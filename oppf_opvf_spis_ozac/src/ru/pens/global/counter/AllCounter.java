package ru.pens.global.counter;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

/**
 * Created by Admin on 21.04.2017.
 */
public class AllCounter {

    private AllCounter(){

    }

    private static class HelpAllCounter{
        private static AllCounter allCounter = new AllCounter();
    }
    public static AllCounter allCounter(){
        return HelpAllCounter.allCounter;
    }

    public int receivingCount(){
        int allCount = 0;
        try{
            Properties properties = new Properties();
            properties.load(new FileInputStream("D:\\PENS_NEW\\Number.properties"));
            allCount = Integer.valueOf(properties.getProperty("outputNumber"));
        } catch (Exception e1){
            JOptionPane.showMessageDialog(null, "Проверьте наличие файла по пути: D:\\PENS_NEW\\Number.properties. Программа закрывается!", "Message header", JOptionPane.DEFAULT_OPTION);
            System.exit(0);
        }
        return allCount;
    }

    public void recordCount(int allCount){
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("D:\\PENS_NEW\\Number.properties"));

            properties.setProperty("outputNumber", String.valueOf(allCount));
            properties.store(new FileOutputStream("D:\\PENS_NEW\\Number.properties"), "");
        } catch (Exception e2){
            JOptionPane.showMessageDialog(null, "Невозможно записать в файл: D:\\PENS_NEW\\Number.properties. Программа закрывается!", "Message header", JOptionPane.DEFAULT_OPTION);
        }

    }
}
