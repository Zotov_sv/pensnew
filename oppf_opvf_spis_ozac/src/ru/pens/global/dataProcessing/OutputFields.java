package ru.pens.global.dataProcessing;

import ru.pens.oppf_opvf.parsOppf.Oppf;
import ru.pens.oppf_opvf.dataProcessing.ParsOppf;
import ru.pens.spis_ozac.dataProcessing.FieldsInMyFrame;
import ru.pens.spis_ozac.dataProcessing.ParsSpis;
import ru.pens.spis_ozac.dataProcessing.Person;
import ru.pens.spis_ozac.parsSpis.Spis;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by I on 22.12.2016.
 */
public class OutputFields {

    Spis originalSpis = ParsSpis.getInputFields().getSpis();
    LinkedHashMap<String, Person> map = FieldsInMyFrame.getMainXmlHelp().getMap();
    Oppf parsOppf = ParsOppf.getOppf();

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
    SimpleDateFormat simpleTimeFormat =new SimpleDateFormat("HH:mm",Locale.getDefault());
    Date date = new Date();

    private String signArraysOrder = parsOppf.getOppfInputDoc().getInputInventory().getSignArrayOrder();

    public String getSignArraysOrder() {
        return signArraysOrder;
    }

    public String systemNumberArray = parsOppf.getOppfInputDoc().getInputInventory().getSystemNumberArray();

    public String getSystemNumberArray() {
        return systemNumberArray;
    }

    private String dateFormation = simpleDateFormat.format(date);

    public String getDateFormation() {
        return dateFormation;
    }

    private String timeFormation = simpleTimeFormat.format(date);

    public String getTimeFormation() {
        return timeFormation;
    }

    private String signDocuments = "ОТЧЕТ_О_ЗАЧИСЛЕНИИ_И_НЕ_ЗАЧИСЛЕНИИ_СУММ";

    public String getSignDocuments() {
        return signDocuments;
    }

    private String numberBankAndMessage = originalSpis.getSpisInputDoc().getSpisInputInventory().getNumberBank();

    public String getNumberBankAndMessage() {
        return numberBankAndMessage;
    }

    private String month = originalSpis.getSpisInputDoc().getSpisInputInventory().getMonth();

    public String getMonth() {
        return month;
    }

    private String year = originalSpis.getSpisInputDoc().getSpisInputInventory().getYear();

    public String getYear() {
        return year;
    }

    private double amountEnrolledFilial = originalSpis.getSpisInputDoc().getSpisInputInventory().getTotalAmount();

    public double getAmountEnrolledFilial() {
        return amountEnrolledFilial;
    }

    private int amountRecipientEnrolled = originalSpis.getSpisInputDoc().getSpisInputInventory().getTotalOrderArrays();

    public int getAmountRecipientEnrolled() {
        return amountRecipientEnrolled;
    }

    private double amountNotEnrolledFilial = 0;

    public double getAmountNotEnrolledFilial() {
        return amountNotEnrolledFilial;
    }

    private int amountRecipientNotEnrolled  = 0;

    public int getAmountRecipientNotEnrolled() {
        return amountRecipientNotEnrolled;
    }

    private String position = "Управляющий Нижнетагильским филиалом АКБ Мосуралбанк (АО)";

    public String getPosition() {
        return position;
    }

    private String head = "Павлова Г.В.";

    public String getHead() {
        return head;
    }

    private String authorityPfrNameCompany = originalSpis.getSpisInputDoc().getSpisInputInventory().
            getSpisInputInventoryTerritorialAuthorityList().iterator().next().
                                                                            getNameOfCompany();

    public String getAuthorityPfrNameCompany() {
        return authorityPfrNameCompany;
    }

    private String authorityPfrInn = originalSpis.getSpisInputDoc().getSpisInputInventory().
            getSpisInputInventoryTerritorialAuthorityList().iterator().next().
            getSpisInputInventoryCompiliedPacksTaxNumber().getINN();

    public String getAuthorityPfrInn() {
        return authorityPfrInn;
    }

    private String authorityPfrKpp = originalSpis.getSpisInputDoc().getSpisInputInventory().
            getSpisInputInventoryTerritorialAuthorityList().iterator().next().
            getSpisInputInventoryCompiliedPacksTaxNumber().getKPP();

    public String getAuthorityPfrKpp() {
        return authorityPfrKpp;
    }

    private String nameOfCompany = "АКБ МОСУРАЛБАНК (АО)";
    //private String nameOfCompany = originalSpis.getSpisInputDoc().getSpisInputInventory().getSpisInputInventoryBank().getNameCompany();
    public String getNameOfCompany() {
        return nameOfCompany;
    }
    private String authorityPfrRegistrationNumber = originalSpis.getSpisInputDoc().getSpisInputInventory().getSpisInputInventoryTerritorialAuthorityList().iterator().next().getRegistrationNumber();
    //private String authorityPfrRegistrationNumber = originalSpis.getSpisInputDoc().getSpisInputInventory().getSpisInputInventoryBank().getRegistrationNumber();
    public String getAuthorityPfrRegistrationNumber() {
        return authorityPfrRegistrationNumber;
    }

    public OutputFields() {

        for (Map.Entry x : map.entrySet()) {
            Person person = (Person) x.getValue();
            if (!person.enrollmentCode.equals("31") & !person.enrollmentCode.equals("32")) {
                amountEnrolledFilial = amountEnrolledFilial - person.deliveredSum;
                amountRecipientNotEnrolled++;
                if (amountEnrolledFilial<=0){
                    amountEnrolledFilial=0;
                }
            }
        }
        amountRecipientEnrolled = amountRecipientEnrolled - amountRecipientNotEnrolled;
        amountNotEnrolledFilial = originalSpis.getSpisInputDoc().getSpisInputInventory().getTotalAmount() - amountEnrolledFilial;
    }


}
