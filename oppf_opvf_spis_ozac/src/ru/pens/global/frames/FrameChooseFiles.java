package ru.pens.global.frames;

import ru.pens.oppf_opvf.dataProcessing.ParsOppf;
import ru.pens.spis_ozac.dataProcessing.FieldsInMyFrame;
import ru.pens.spis_ozac.dataProcessing.ParsSpis;
import ru.pens.spis_ozac.frames.CloseableFrame;
import ru.pens.spis_ozac.frames.FrameCustomerData;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.UnmarshalException;
import java.util.ArrayList;

/**
 * Created by Admin on 27.01.2017.
 */
public class FrameChooseFiles extends JFrame implements CloseableFrame {
    private JPanel jPanel;
    private JPanel jPanelButton;
    private JPanel jPanelFields;
    private JTextField jTextChooseFiles;
    private JTextField jTextOutputFiles;
    private JButton jButtonChooseFiles;
    private JButton JButtonOutputFiles;
    private JPanel jPanelTitle;
    private JLabel jLabelTitle;
    private JPanel jPanelButtonNextView;
    private JButton jButtonNextView;
    private ArrayList<String> listArray = new ArrayList<>();
    private static int allCounter;
    private static ArrayList<String> listNameInputFiles = new ArrayList<>();

    public FrameChooseFiles() {

        jButtonChooseFiles.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser jFileChooserInput = new JFileChooser();
                jFileChooserInput.showDialog(null, "Открыть файл");
                jTextChooseFiles.setText(String.valueOf(jFileChooserInput.getSelectedFile()));
            }
        });
        JButtonOutputFiles.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser jFileChooserOutput = new JFileChooser();

                jFileChooserOutput.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                jFileChooserOutput.setAcceptAllFileFilterUsed(false);
                jFileChooserOutput.showDialog(null, "Выбрать папку");
                jTextOutputFiles.setText(String.valueOf(jFileChooserOutput.getSelectedFile()));
            }
        });

        jButtonNextView.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {

                    ParsOppf.pars(jTextChooseFiles.getText());
                    listNameInputFiles.add(ParsOppf.getOppf().getNameFile());
                    //listNameInputFiles.add(ParsOppf.getOppf().getNameFile());
                    //allCounter=AllCounter.allCounter().receivingCount();
                    setVisible(false);
                    close(-1);
                }
                 catch (UnmarshalException e2) {
                    JOptionPane.showMessageDialog(null, "Файл не опознан,пожалуйста выберите новый файл", "Нет заголовка <{}ФайлПФР>", JOptionPane.DEFAULT_OPTION);
                    setVisible(true);

                }

                catch (Exception e3){

                }



            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        add(jPanel);
        setVisible(true);
        pack();
        setLocationRelativeTo(null);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    @Override
    public void close(int position) {
        //setVisible(true);

        try {
            //ParsOppf.pars(jTextChooseFiles.getText());
            Path path = Paths.get(jTextChooseFiles.getText());

            String name = ParsOppf.getOppf().getOppfInputDoc().getTransferredFiles().getTrFiles().get(position + 1).getNameFile();
            listNameInputFiles.add(name);
            if (ParsOppf.getOppf().getOppfInputDoc().getTransferredFiles().getTrFiles().size() > position + 1) {
                ParsSpis.getInputFields().pars(path.getParent() + "\\" + name);
                FieldsInMyFrame.getMainXmlHelp().fieldsInFrame(ParsSpis.getInputFields().getSpis());
                FrameCustomerData fm = new FrameCustomerData(FieldsInMyFrame.getMainXmlHelp().getMap(), FieldsInMyFrame.getMainXmlHelp().getListCodeDelivered(),
                        jTextOutputFiles.getText(), listNameInputFiles);
                //allCounter++;
                fm.setCloseableFrame(FrameChooseFiles.this);
                fm.setCountPosition(position + 1);
            } else {

            }
        } catch (NullPointerException e1) {
            JOptionPane.showMessageDialog(null, "Выбери файл с окончанием OPPF", "Message header", JOptionPane.DEFAULT_OPTION);
            setVisible(true);
        } catch (IndexOutOfBoundsException e3) {
            System.exit(0);
        }
        catch (Exception e) {


            System.out.print(e);
        }


    }
}
