package ru.pens.global.frames;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Admin on 31.01.2017.
 */
public class HilAll extends JFrame {
    private JPanel jPanelMain;
    private JTextArea jTextAreaHi;
    private JPanel jPanelTextArea;
    private JLabel jLabeHeader;
    private JPanel jPanelHeader;
    private JPanel jPanelButton;
    private JButton jButtonOk;
    private JButton jButtonCancel;

    public HilAll() {

        jButtonOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new FrameChooseFiles();
                setVisible(false);
            }
        });

        jButtonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);

            }
        });

        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setBounds(0, 0, 750, 490);
        add(jPanelMain);
        setLocationRelativeTo(null);
    }
}
