package ru.pens.oppf_opvf.createOpvf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Admin on 25.01.2017.
 */

@XmlRootElement(name = "ФайлПФР")
@XmlAccessorType(XmlAccessType.FIELD)
public class Opvf {

    @XmlElement(name = "ИмяФайла")
    private String nameFile;

    @XmlElement(name = "ЗаголовокФайла")
    private OpvfHeaderFile headerFile;

    @XmlElement(name = "ПачкаИсходящихДокументов")
    private OpvfDocs opvfDocs;

    public void setOpvfDocs(OpvfDocs opvfDocs) {
        this.opvfDocs = opvfDocs;
    }

    public void setHeaderFile(OpvfHeaderFile headerFile) {
        this.headerFile = headerFile;
    }

    public void setNameFile(String nameFile) {

        this.nameFile = nameFile;
    }
}