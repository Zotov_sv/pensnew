package ru.pens.oppf_opvf.createOpvf;

import ru.pens.oppf_opvf.parsOppf.OppfInputInventory;

import javax.xml.bind.annotation.*;

/**
 * Created by Admin on 27.01.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OpvfDocs {

    @XmlAttribute (name = "ДоставочнаяОрганизация")
    private String bank = "Банк";

    @XmlElement(name = "ВХОДЯЩАЯ_ОПИСЬ")
    private OppfInputInventory inputInventory;

    @XmlElement (name = "ИСХОДЯЩАЯ_ОПИСЬ")
    private OpvfOutputInventory outputInventory;

    @XmlElement(name = "ОПИСЬ_ВОЗВРАЩАЕМЫХ_ФАЙЛОВ_ПРИ_ЗАЧИСЛЕНИИ")
    private OpvfInventoryReturnedFiles returnedFiles;

    public void setInputInventory(OppfInputInventory inputInventory) {
        this.inputInventory = inputInventory;
    }

    public void setOutputInventory(OpvfOutputInventory outputInventory) {
        this.outputInventory = outputInventory;
    }

    public void setReturnedFiles(OpvfInventoryReturnedFiles returnedFiles) {
        this.returnedFiles = returnedFiles;
    }
}
