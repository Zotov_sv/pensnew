package ru.pens.oppf_opvf.createOpvf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 26.01.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OpvfHeaderFile {

    @XmlElement(name = "ВерсияФормата")
    private String versionFormat;

    @XmlElement (name = "ТипФайла")
    private String typeFile;

    @XmlElement (name = "ПрограммаПодготовкиДанных")
    private OpvfHeaderFileTrainingProgramData programData;

    @XmlElement (name = "ИсточникДанных")
    private String dataSource;

    public void setVersionFormat(String versionFormat) {
        this.versionFormat = versionFormat;
    }

    public void setTypeFile(String typeFile) {
        this.typeFile = typeFile;
    }

    public void setProgramData(OpvfHeaderFileTrainingProgramData programData) {
        this.programData = programData;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }
}
