package ru.pens.oppf_opvf.createOpvf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 26.01.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class OpvfHeaderFileTrainingProgramData {

    @XmlElement(name = "НазваниеПрограммы")
    private String nameOfMyProgram;

    @XmlElement(name = "Версия")
    private String version;

    public String getNameOfMyProgram() {
        return nameOfMyProgram;
    }

    public void setNameOfMyProgram(String nameOfMyProgram) {
        this.nameOfMyProgram = nameOfMyProgram;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
