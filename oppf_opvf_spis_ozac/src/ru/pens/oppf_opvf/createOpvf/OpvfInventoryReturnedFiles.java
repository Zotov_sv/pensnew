package ru.pens.oppf_opvf.createOpvf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;

/**
 * Created by Admin on 27.01.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class OpvfInventoryReturnedFiles {

    @XmlElement (name = "КоличествоПередаваемыхФайлов")
    private int numberTransfferedFiles;

    @XmlElement (name = "ПередаваемыйФайл")
    private ArrayList<OpvfInventoryReturnedFilesTransffered> transfferedFiles;

    public int getNumberTransfferedFiles() {
        return numberTransfferedFiles;
    }

    public void setNumberTransfferedFiles(int numberTransfferedFiles) {
        this.numberTransfferedFiles = numberTransfferedFiles;
    }

    public ArrayList<OpvfInventoryReturnedFilesTransffered> getTransfferedFiles() {
        return transfferedFiles;
    }

    public void setTransfferedFiles(ArrayList<OpvfInventoryReturnedFilesTransffered> transfferedFiles) {
        this.transfferedFiles = transfferedFiles;
    }
}
