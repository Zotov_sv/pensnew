package ru.pens.oppf_opvf.createOpvf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 27.01.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OpvfInventoryReturnedFilesTransffered {

    @XmlElement(name = "ТипДокумента")
    private String typeDocument;

    @XmlElement(name = "ИмяФайла")
    private String nameFile;

    @XmlElement(name = "КоличествоПолучателей")
    private String  amountRecipient;

    @XmlElement(name = "НомерЧастиМассива")
    private String numberPartArray;

    @XmlElement (name = "СуммаПоЧастиМассива")
    private String  amountPartArray;

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public void setAmountRecipient(String amountRecipient) {
        this.amountRecipient = amountRecipient;
    }

    public void setNumberPartArray(String  numberPartArray) {
        this.numberPartArray = numberPartArray;
    }

    public void setAmountPartArray(String amountPartArray) {
        this.amountPartArray = amountPartArray;
    }
}
