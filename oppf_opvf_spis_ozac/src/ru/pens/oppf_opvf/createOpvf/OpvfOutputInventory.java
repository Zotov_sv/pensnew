package ru.pens.oppf_opvf.createOpvf;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 27.01.2017.
 */


@XmlAccessorType(XmlAccessType.FIELD)
public class OpvfOutputInventory {

    @XmlElement(name = "СоставительПачки")
    private OpvfOutputInventoryCompiledPacks compiliedPacks;

    @XmlElement (name = "СоставДокументов")
    private OpvfOutputInventoryTheDocuments opvfOutputInventoryTheDocuments;

    @XmlElement (name = "ТерриториальныйОрганПФР")
    private OpvfOutputInventoryTerritorialAuthorityPFR authorityPFR;

    @XmlElement (name = "НомерБанка")
    private String numberBank;

    @XmlElement (name = "ОрганизацияСформировавшаяДокумент")
    private OpvfOutputInventoryCompanyFormedDocuments companyFormedDocuments;

    @XmlElement (name = "ТипМассиваПоручений")
    private String signArraysOrder;

    @XmlElement (name = "Месяц")
    private String month;

    @XmlElement (name = "Год")
    private String year;

    @XmlElement (name = "СуммаЗачисленоПоФилиалу")
    private String amountFilial;

    @XmlElement (name = "КоличествоПолучателейЗачислено")
    private String recipientEnrolled;

    @XmlElement (name = "СуммаНеЗачисленоПоФилиалу")
    private String notAmountFilial;

    @XmlElement (name = "КоличествоПолучателейНеЗачислено")
    private String recipientNotEnrolled;

    @XmlElement (name = "ИсходящийНомер")
    private String ouputNumber;

    @XmlElement (name = "ДатаФормирования")
    private String dateFormation;

    @XmlElement (name = "Должность")
    private String position;

    @XmlElement (name = "Руководитель")
    private String head;

    public void setCompiliedPacks(OpvfOutputInventoryCompiledPacks compiliedPacks) {
        this.compiliedPacks = compiliedPacks;
    }

    public void setOpvfOutputInventoryTheDocuments(OpvfOutputInventoryTheDocuments opvfOutputInventoryTheDocuments) {
        this.opvfOutputInventoryTheDocuments = opvfOutputInventoryTheDocuments;
    }

    public void setAuthorityPFR(OpvfOutputInventoryTerritorialAuthorityPFR authorityPFR) {
        this.authorityPFR = authorityPFR;
    }

    public void setNumberBank(String numberBank) {
        this.numberBank = numberBank;
    }

    public void setCompanyFormedDocuments(OpvfOutputInventoryCompanyFormedDocuments companyFormedDocuments) {
        this.companyFormedDocuments = companyFormedDocuments;
    }

    public void setSignArraysOrder(String signArraysOrder) {
        this.signArraysOrder = signArraysOrder;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setAmountFilial(String amountFilial) {
        this.amountFilial = amountFilial;
    }

    public void setRecipientEnrolled(String recipientEnrolled) {
        this.recipientEnrolled = recipientEnrolled;
    }

    public void setNotAmountFilial(String notAmountFilial) {
        this.notAmountFilial = notAmountFilial;
    }

    public void setRecipientNotEnrolled(String recipientNotEnrolled) {
        this.recipientNotEnrolled = recipientNotEnrolled;
    }

    public void setOuputNumber(String ouputNumber) {
        this.ouputNumber = ouputNumber;
    }

    public void setDateFormation(String dateFormation) {
        this.dateFormation = dateFormation;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setHead(String head) {
        this.head = head;
    }
}
