package ru.pens.oppf_opvf.createOpvf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 01.02.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class OpvfOutputInventoryCompanyFormedDocuments {

        @XmlElement(name = "НаименованиеОрганизации")
        private String nameOfCompany;

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }
}
