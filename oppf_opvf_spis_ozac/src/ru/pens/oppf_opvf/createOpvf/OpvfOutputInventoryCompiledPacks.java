package ru.pens.oppf_opvf.createOpvf;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 01.02.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class OpvfOutputInventoryCompiledPacks {

    @XmlElement(name = "НалоговыйНомер")
    private OpvfOutputInventoryCompiledPacksTaxNumber packsTaxNumber;

    @XmlElement (name = "НаименованиеОрганизации")
    private String nameOfCompany;

    @XmlElement(name = "РегистрационныйНомер")
    private String  packsRegistrationNumber;

    @XmlElement(name = "Подразделение")
    private OpvfOutputInventoryCompiledPacksUnit compiledPacksUnit;


    public void setPacksTaxNumber(OpvfOutputInventoryCompiledPacksTaxNumber packsTaxNumber) {
        this.packsTaxNumber = packsTaxNumber;
    }

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }

    public void setPacksRegistrationNumber(String packsRegistrationNumber) {
        this.packsRegistrationNumber = packsRegistrationNumber;
    }

    public void setCompiledPacksUnit(OpvfOutputInventoryCompiledPacksUnit compiledPacksUnit) {
        this.compiledPacksUnit = compiledPacksUnit;
    }

}
