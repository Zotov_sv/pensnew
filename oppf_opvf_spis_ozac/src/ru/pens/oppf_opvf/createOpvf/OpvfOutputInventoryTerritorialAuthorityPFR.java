package ru.pens.oppf_opvf.createOpvf;

import ru.pens.spis_ozac.createOzac.OzacOutputInventoryTerritorialAuthorityPFRTaxNumber;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 01.02.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OpvfOutputInventoryTerritorialAuthorityPFR {

    @XmlElement(name = "НалоговыйНомер")
    private OpvfOutputInventoryTerritorialAuthorityPFRTaxNumber pfrTaxNumber;

    @XmlElement(name = "НаименованиеОрганизации")
    private String nameOfCompany;

    @XmlElement(name = "РегистрационныйНомер")
    private String registrationNumber;

    @XmlElement(name = "Подразделение")
    private OpvfOutputInventoryTerritorialAuthorityPFRTaxNumberUnit authorityPFRTaxNumberUnit;

    public void setPfrTaxNumber(OpvfOutputInventoryTerritorialAuthorityPFRTaxNumber pfrTaxNumber) {
        this.pfrTaxNumber = pfrTaxNumber;
    }

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public void setAuthorityPFRTaxNumberUnit(OpvfOutputInventoryTerritorialAuthorityPFRTaxNumberUnit authorityPFRTaxNumberUnit) {
        this.authorityPFRTaxNumberUnit = authorityPFRTaxNumberUnit;
    }
}
