package ru.pens.oppf_opvf.createOpvf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 01.02.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OpvfOutputInventoryTerritorialAuthorityPFRTaxNumber {

        @XmlElement(name = "ИНН")
        private String inn;

        @XmlElement (name = "КПП")
        private String kpp;

    public void setInn(String inn) {
        this.inn = inn;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }
}
