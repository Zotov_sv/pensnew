package ru.pens.oppf_opvf.createOpvf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 01.02.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class OpvfOutputInventoryTerritorialAuthorityPFRTaxNumberUnit {

        @XmlElement(name = "НаименованиеПодразделения")
        private String nameUnit;

        @XmlElement (name = "НомерПодразделения")
        private String numberUnit;

    public void setNameUnit(String nameUnit) {
        this.nameUnit = nameUnit;
    }

    public void setNumberUnit(String numberUnit) {
        this.numberUnit = numberUnit;
    }
}
