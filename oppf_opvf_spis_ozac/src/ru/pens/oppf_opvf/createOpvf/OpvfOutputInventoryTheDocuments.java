package ru.pens.oppf_opvf.createOpvf;

import ru.pens.spis_ozac.createOzac.OzacOutputInventoryTheDocumentsAvailabilityOfDocuments;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by Admin on 01.02.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OpvfOutputInventoryTheDocuments {

    @XmlElement(name = "Количество")
    private int quantity;

    @XmlElement(name = "НаличиеДокументов")
    private List<OpvfOutputInventoryTheDocumentsAvailabilityOfDocuments> availabilityOfDocuments;

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setAvailabilityOfDocuments(List<OpvfOutputInventoryTheDocumentsAvailabilityOfDocuments> availabilityOfDocuments) {
        this.availabilityOfDocuments = availabilityOfDocuments;
    }
}
