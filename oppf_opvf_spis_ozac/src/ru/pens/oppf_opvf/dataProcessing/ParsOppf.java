package ru.pens.oppf_opvf.dataProcessing;

import ru.pens.oppf_opvf.createOpvf.Opvf;
import ru.pens.oppf_opvf.parsOppf.Oppf;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by Admin on 25.01.2017.
 */
public class ParsOppf {

    private static Oppf oppf;

    public static void main(String[] args) throws Exception {
        //pars("E:\\PENS_NEW\\zipSEDfilesBank59552\\PFR-700-Y-2016-ORG-075-030-120201-DIS-038-DCK-59552-000-DOC-OPPF-FSB-0000.XML");
    }

    public static void pars(String pathFile) throws Exception {
        try {

            JAXBContext jc = JAXBContext.newInstance(Oppf.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            oppf = (Oppf) unmarshaller.unmarshal(new File(pathFile));

        } catch (Exception e) {
            System.out.print(e);
        }
    }

    public static Oppf getOppf() {
        return oppf;
    }
}
