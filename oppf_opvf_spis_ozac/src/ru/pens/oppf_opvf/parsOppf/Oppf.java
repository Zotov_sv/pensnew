package ru.pens.oppf_opvf.parsOppf;



import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Admin on 25.01.2017.
 */
@XmlRootElement(name = "ФайлПФР")
@XmlAccessorType(XmlAccessType.FIELD)

public class Oppf {

    @XmlElement(name = "ИмяФайла")
    private String nameFile;

    @XmlElement(name = "ЗаголовокФайла")
    private OppfHeaderFile headerFile;

    @XmlElement(name = "ПачкаВходящихДокументов")
    private OppfInputDoc oppfInputDoc;

    public String getNameFile() {
        return nameFile;
    }

    public OppfHeaderFile getHeaderFile() {
        return headerFile;
    }

    public OppfInputDoc getOppfInputDoc() {
        return oppfInputDoc;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public void setHeaderFile(OppfHeaderFile headerFile) {
        this.headerFile = headerFile;
    }

    public void setOppfInputDoc(OppfInputDoc oppfInputDoc) {
        this.oppfInputDoc = oppfInputDoc;
    }

 }

