package ru.pens.oppf_opvf.parsOppf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 26.01.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OppfHeaderFile {

        @XmlElement(name = "ВерсияФормата")
        private String versionFormat;

        @XmlElement (name = "ТипФайла")
        private String typeFile;

        @XmlElement (name = "ПрограммаПодготовкиДанных")
        private OppfHeaderFileTrainingProgramData programData;

        @XmlElement (name = "ИсточникДанных")
        private String dataSource;

        public String getVersionFormat() {
                return versionFormat;
        }

        public void setVersionFormat(String versionFormat) {
                this.versionFormat = versionFormat;
        }

        public String getTypeFile() {
                return typeFile;
        }

        public void setTypeFile(String typeFile) {
                this.typeFile = typeFile;
        }

        public OppfHeaderFileTrainingProgramData getProgramData() {
                return programData;
        }

        public void setProgramData(OppfHeaderFileTrainingProgramData programData) {
                this.programData = programData;
        }

        public String getDataSource() {
                return dataSource;
        }

        public void setDataSource(String dataSource) {
                this.dataSource = dataSource;
        }
}
