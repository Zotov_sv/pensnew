package ru.pens.oppf_opvf.parsOppf;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * Created by Admin on 09.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OppfInputDoc {

    @XmlElement(name = "ВХОДЯЩАЯ_ОПИСЬ")
    private OppfInputInventory inputInventory;

    @XmlElement(name = "ОПИСЬ_ПЕРЕДАВАЕМЫХ_ФАЙЛОВ_НА_ЗАЧИСЛЕНИЕ")
    private OppfListTransferredFilesEnrollment transferredFiles;

    public OppfInputInventory getInputInventory() {
        return inputInventory;
    }

    public void setOppfInputInventory(OppfInputInventory inputInventory) {
        this.inputInventory = inputInventory;
    }

    public OppfListTransferredFilesEnrollment getTransferredFiles() {
        return transferredFiles;
    }

    public void setTransferredFiles(OppfListTransferredFilesEnrollment transferredFiles) {
        this.transferredFiles = transferredFiles;
    }

}
