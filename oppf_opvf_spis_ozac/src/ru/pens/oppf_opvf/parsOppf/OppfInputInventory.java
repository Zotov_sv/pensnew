package ru.pens.oppf_opvf.parsOppf;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by Admin on 25.01.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OppfInputInventory {

    @XmlElement(name = "НомерВпачке")
    private int numberStack;

    @XmlElement(name = "ТипВходящейОписи")
    private String signInventory;

    @XmlElement(name = "СоставительПачки")
    private OppfInputInventoryCompiliedPacks inputInventoryCompiliedPacksList;

    @XmlElement(name = "НомерПачки")
    private OppfInputInventoryTwinPack inputInventoryTwinPack;

    @XmlElement(name = "СоставДокументов")
    private List<OppfInputInventoryDocumentComposition> inputInventoryDocumentCompositionList;

    @XmlElement(name = "ДатаСоставления")
    private String DateOfPreparation;

    @XmlElement(name = "ТерриториальныйОрганПФР")
    private OppfInputInventoryTerritorialAuthority inputInventoryTerritorialAuthorityList;

    @XmlElement(name = "НомерБанка")
    private String numberBank;

    @XmlElement(name = "Банк")
    private OppfInputInventoryBank inputInventoryBank;

    @XmlElement(name = "СистемныйНомерМассива")
    private String systemNumberArray;

    @XmlElement(name = "ТипМассиваПоручений")
    private String signArrayOrder;

    @XmlElement(name = "Месяц")
    private String month;

    @XmlElement(name = "Год")
    private String year;

    @XmlElement(name = "ОбщаяСуммаПоМассиву")
    private double totalAmount;

    @XmlElement(name = "ОбщееКоличествоПорученийПоМассиву")
    private int totalOrderArrays;

    @XmlElement(name = "КоличествоЧастейМассива")
    private String quantityArrays;

    @XmlElement(name = "Должность")
    private String position;

    @XmlElement(name = "Руководитель")
    private String head;

    public int getNumberStack() {
        return numberStack;
    }

    public void setNumberStack(int numberStack) {
        this.numberStack = numberStack;
    }

    public String getSignInventory() {
        return signInventory;
    }

    public void setSignInventory(String signInventory) {
        this.signInventory = signInventory;
    }

    public OppfInputInventoryCompiliedPacks getInputInventoryCompiliedPacksList() {
        return inputInventoryCompiliedPacksList;
    }

    public void setInputInventoryCompiliedPacksList(OppfInputInventoryCompiliedPacks inputInventoryCompiliedPacksList) {
        this.inputInventoryCompiliedPacksList = inputInventoryCompiliedPacksList;
    }

    public OppfInputInventoryTwinPack getInputInventoryTwinPack() {
        return inputInventoryTwinPack;
    }

    public void setInputInventoryTwinPack(OppfInputInventoryTwinPack inputInventoryTwinPack) {
        this.inputInventoryTwinPack = inputInventoryTwinPack;
    }

    public List<OppfInputInventoryDocumentComposition> getInputInventoryDocumentCompositionList() {
        return inputInventoryDocumentCompositionList;
    }

    public void setInputInventoryDocumentCompositionList(List<OppfInputInventoryDocumentComposition> inputInventoryDocumentCompositionList) {
        this.inputInventoryDocumentCompositionList = inputInventoryDocumentCompositionList;
    }

    public String getDateOfPreparation() {
        return DateOfPreparation;
    }

    public void setDateOfPreparation(String dateOfPreparation) {
        DateOfPreparation = dateOfPreparation;
    }

    public OppfInputInventoryTerritorialAuthority getInputInventoryTerritorialAuthorityList() {
        return inputInventoryTerritorialAuthorityList;
    }

    public void setInputInventoryTerritorialAuthorityList(OppfInputInventoryTerritorialAuthority inputInventoryTerritorialAuthorityList) {
        this.inputInventoryTerritorialAuthorityList = inputInventoryTerritorialAuthorityList;
    }

    public String getNumberBank() {
        return numberBank;
    }

    public void setNumberBank(String numberBank) {
        this.numberBank = numberBank;
    }

    public OppfInputInventoryBank getInputInventoryBank() {
        return inputInventoryBank;
    }

    public void setInputInventoryBank(OppfInputInventoryBank inputInventoryBank) {
        this.inputInventoryBank = inputInventoryBank;
    }

    public String getSystemNumberArray() {
        return systemNumberArray;
    }

    public void setSystemNumberArray(String systemNumberArray) {
        this.systemNumberArray = systemNumberArray;
    }

    public String getSignArrayOrder() {
        return signArrayOrder;
    }

    public void setSignArrayOrder(String signArrayOrder) {
        this.signArrayOrder = signArrayOrder;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getTotalOrderArrays() {
        return totalOrderArrays;
    }

    public void setTotalOrderArrays(int totalOrderArrays) {
        this.totalOrderArrays = totalOrderArrays;
    }

    public String getQuantityArrays() {
        return quantityArrays;
    }

    public void setQuantityArrays(String quantityArrays) {
        this.quantityArrays = quantityArrays;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }
}
