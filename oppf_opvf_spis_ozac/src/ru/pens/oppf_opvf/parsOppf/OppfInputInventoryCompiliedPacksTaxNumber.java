package ru.pens.oppf_opvf.parsOppf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 25.01.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class OppfInputInventoryCompiliedPacksTaxNumber {

    @XmlElement(name = "ИНН")
    private String INN;

    @XmlElement(name = "КПП")
    private String KPP;

    public String getINN() {
        return INN;
    }

    public void setINN(String INN) {
        this.INN = INN;
    }

    public String getKPP() {
        return KPP;
    }

    public void setKPP(String KPP) {
        this.KPP = KPP;
    }
}
