package ru.pens.oppf_opvf.parsOppf;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * Created by Admin on 25.01.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OppfInputInventoryDocumentComposition {

    @XmlElement(name = "Количество")
    private int quantity;

    @XmlElement(name = "НаличиеДокументов")
    private OppfInputInventoryDocumentCompositionAvailabilityOfDocuments availabilityOfDocumentsList;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public OppfInputInventoryDocumentCompositionAvailabilityOfDocuments getAvailabilityOfDocumentsList() {
        return availabilityOfDocumentsList;
    }

    public void setAvailabilityOfDocumentsList(OppfInputInventoryDocumentCompositionAvailabilityOfDocuments availabilityOfDocumentsList) {
        this.availabilityOfDocumentsList = availabilityOfDocumentsList;
    }
}
