package ru.pens.oppf_opvf.parsOppf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 25.01.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OppfInputInventoryDocumentCompositionAvailabilityOfDocuments {

    @XmlElement(name = "ТипДокумента")
    private String signDocument;

    @XmlElement(name = "Количество")
    private int quantity;

    public String getSignDocument() {
        return signDocument;
    }

    public void setSignDocument(String signDocument) {
        this.signDocument = signDocument;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

