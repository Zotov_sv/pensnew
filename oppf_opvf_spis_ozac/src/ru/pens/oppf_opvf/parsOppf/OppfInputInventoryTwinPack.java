package ru.pens.oppf_opvf.parsOppf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 25.01.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OppfInputInventoryTwinPack {

    @XmlElement(name = "Основной")
    private int main;

    public int getMain() {
        return main;
    }

    public void setMain(int main) {
        this.main = main;
    }
}
