package ru.pens.oppf_opvf.parsOppf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 25.01.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OppfListTransferredFilesEnrollment {
    @XmlElement(name = "НомерВпачке")
    private int numberStack;

    @XmlElement(name = "КоличествоПередаваемыхФайлов")
    private int numberTransferredFiles;

    @XmlElement(name = "ПередаваемыйФайл")
    private List<OppfListTransferredFilesEnrollmentTrFiles> trFiles;

    public int getNumberStack() {
        return numberStack;
    }

    public void setNumberStack(int numberStack) {
        this.numberStack = numberStack;
    }

    public int getNumberTransferredFiles() {
        return numberTransferredFiles;
    }

    public void setNumberTransferredFiles(int numberTransferredFiles) {
        this.numberTransferredFiles = numberTransferredFiles;
    }

    public List<OppfListTransferredFilesEnrollmentTrFiles> getTrFiles() {
        return trFiles;
    }

    public void setTrFiles(List<OppfListTransferredFilesEnrollmentTrFiles> trFiles) {
        this.trFiles = trFiles;
    }
}
