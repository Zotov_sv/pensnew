package ru.pens.oppf_opvf.parsOppf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 25.01.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OppfListTransferredFilesEnrollmentTrFiles {

    @XmlElement(name = "ТипДокумента")
    private String documentType;

    @XmlElement(name = "ИмяФайла")
    private String nameFile;

    @XmlElement(name = "КоличествоПолучателей")
    private int numberRecipient;

    @XmlElement(name = "НомерЧастиМассива")
    private int partNumberArray;

    @XmlElement(name = "СуммаПоЧастиМассива")
    private double partAmountArray;

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getNameFile() {
        return nameFile;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public int getNumberRecipient() {
        return numberRecipient;
    }

    public void setNumberRecipient(int numberRecipient) {
        this.numberRecipient = numberRecipient;
    }

    public int getPartNumberArray() {
        return partNumberArray;
    }

    public void setPartNumberArray(int partNumberArray) {
        this.partNumberArray = partNumberArray;
    }

    public double getPartAmountArray() {
        return partAmountArray;
    }

    public void setPartAmountArray(double partAmountArray) {
        this.partAmountArray = partAmountArray;
    }
}
