package ru.pens.oppf_opvf.withFiles;

import ru.pens.oppf_opvf.createOpvf.*;
import ru.pens.oppf_opvf.dataProcessing.ParsOppf;
import ru.pens.oppf_opvf.parsOppf.OppfListTransferredFilesEnrollmentTrFiles;
import ru.pens.posd.withFiles.PosdBase;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Admin on 26.01.2017.
 */

public class OpvfBaseXml {
    private int count = 0;

    Opvf opvf = new Opvf();

    public OpvfBaseXml(String nameFileOzac, HashMap<String,
            String> hashMap, String outputPath,
                       ArrayList<String> listNameInputFiles,
                       ArrayList<String> listNameOutputFiles,ArrayList<String> listNameOutputFilesSpra,
                       int numberStack,int mainNumberStack,int tmpCountSpra,int outputNumber) {

        //countSpra = 0+tmpCountSpra;
        /*Блок для заголовка файла*/
        OpvfHeaderFile headerFile = new OpvfHeaderFile();
        headerFile.setVersionFormat("07.00");
        headerFile.setTypeFile("ВНЕШНИЙ");
        OpvfHeaderFileTrainingProgramData trainingProgramData = new OpvfHeaderFileTrainingProgramData();
        trainingProgramData.setNameOfMyProgram("МосуралАйтиПродакшн");
        trainingProgramData.setVersion("ВЕРСИЯ1.12");
        headerFile.setProgramData(trainingProgramData);
        headerFile.setDataSource("ДОСТАВЩИК");
        opvf.setHeaderFile(headerFile);
        /******************************/

            /*Блок для ВходящейОписи*/
        OpvfDocs opvfDocs = new OpvfDocs();
        opvfDocs.setInputInventory(ParsOppf.getOppf().getOppfInputDoc().getInputInventory());
        /******************************/

            /*Блок для ИсходящейОписи*/
        OpvfOutputInventory outputInventory = new OpvfOutputInventory();
        OpvfOutputInventoryCompiledPacks compiledPacks = new OpvfOutputInventoryCompiledPacks();
        OpvfOutputInventoryCompiledPacksTaxNumber compiledPacksTaxNumber = new OpvfOutputInventoryCompiledPacksTaxNumber();
        compiledPacksTaxNumber.setInn("7707083011");
        compiledPacksTaxNumber.setKpp("662343001");
        compiledPacks.setPacksTaxNumber(compiledPacksTaxNumber);
        compiledPacks.setNameOfCompany(hashMap.get("Name Company"));
        compiledPacks.setPacksRegistrationNumber(hashMap.get("Registration Number"));
        OpvfOutputInventoryCompiledPacksUnit compiledPacksUnit = new OpvfOutputInventoryCompiledPacksUnit();
        compiledPacksUnit.setNameUnit(hashMap.get("Unit Company"));
        compiledPacksUnit.setNumberUnit(hashMap.get("Unit RegistrationNumber"));
        compiledPacks.setCompiledPacksUnit(compiledPacksUnit);
        outputInventory.setCompiliedPacks(compiledPacks);
        OpvfOutputInventoryTheDocuments inventoryTheDocuments = new OpvfOutputInventoryTheDocuments();
        /*inventoryTheDocuments.setQuantity(ParsOppf.getOppf().getOppfInputDoc().getInputInventory().
                getInputInventoryDocumentCompositionList().iterator().next().getQuantity());*/
        inventoryTheDocuments.setQuantity(listNameOutputFiles.size()+listNameOutputFilesSpra.size());
        List<OpvfOutputInventoryTheDocumentsAvailabilityOfDocuments> availabilityOfDocuments = new ArrayList<OpvfOutputInventoryTheDocumentsAvailabilityOfDocuments>();
        /*availabilityOfDocuments.setQuantity(ParsOppf.getOppf().getOppfInputDoc().getInputInventory().
                getInputInventoryDocumentCompositionList().iterator().next().getQuantity());*/
        //availabilityOfDocuments.setQuantity(listNameOutputFiles.size());

        if (tmpCountSpra >= 1){
            for (int i = 0; i < 1; i++){
                int tmp = 1;
                OpvfOutputInventoryTheDocumentsAvailabilityOfDocuments listAvailabilityOfDocuments = new OpvfOutputInventoryTheDocumentsAvailabilityOfDocuments();
                listAvailabilityOfDocuments.setSignDocument("ОПИСЬ_ВОЗВРАЩАЕМЫХ_ФАЙЛОВ_ПРИ_ЗАЧИСЛЕНИИ");
                listAvailabilityOfDocuments.setQuantity(listNameOutputFiles.size());
                availabilityOfDocuments.add(listAvailabilityOfDocuments);
                tmp++;
            }
            for (int i = 0;i < 1;i++){
                int tmp = 1;
                OpvfOutputInventoryTheDocumentsAvailabilityOfDocuments listAvailabilityOfDocuments = new OpvfOutputInventoryTheDocumentsAvailabilityOfDocuments();
                listAvailabilityOfDocuments.setSignDocument("СПИСОК_РАСХОЖДЕНИЙ");
                listAvailabilityOfDocuments.setQuantity(listNameOutputFilesSpra.size());
                availabilityOfDocuments.add(listAvailabilityOfDocuments);
                //inventoryTheDocuments.setAvailabilityOfDocuments(availabilityOfDocuments);
                tmp++;
            }
        }
        else {

            for (int i = 0; i < 1; i++){
                int tmp = 1;
                OpvfOutputInventoryTheDocumentsAvailabilityOfDocuments listAvailabilityOfDocuments = new OpvfOutputInventoryTheDocumentsAvailabilityOfDocuments();
                listAvailabilityOfDocuments.setSignDocument("ОПИСЬ_ВОЗВРАЩАЕМЫХ_ФАЙЛОВ_ПРИ_ЗАЧИСЛЕНИИ");
                listAvailabilityOfDocuments.setQuantity(listNameOutputFiles.size());
                availabilityOfDocuments.add(listAvailabilityOfDocuments);
                tmp++;
            }
        }



        inventoryTheDocuments.setAvailabilityOfDocuments(availabilityOfDocuments);
        outputInventory.setOpvfOutputInventoryTheDocuments(inventoryTheDocuments);
        OpvfOutputInventoryTerritorialAuthorityPFR inventoryTerritorialAuthorityPFR = new OpvfOutputInventoryTerritorialAuthorityPFR();
        OpvfOutputInventoryTerritorialAuthorityPFRTaxNumber authorityPFRTaxNumber = new OpvfOutputInventoryTerritorialAuthorityPFRTaxNumber();
        authorityPFRTaxNumber.setInn(hashMap.get("AuthorityPfrInn"));
        authorityPFRTaxNumber.setKpp(hashMap.get("AuthorityPfrKpp"));
        inventoryTerritorialAuthorityPFR.setPfrTaxNumber(authorityPFRTaxNumber);
        inventoryTerritorialAuthorityPFR.setNameOfCompany(hashMap.get("AuthorityPfrNameCompany"));
        inventoryTerritorialAuthorityPFR.setRegistrationNumber(hashMap.get("AuthorityPfrRegistrationNumber"));
        OpvfOutputInventoryTerritorialAuthorityPFRTaxNumberUnit pfrTaxNumberUnit = new OpvfOutputInventoryTerritorialAuthorityPFRTaxNumberUnit();
        pfrTaxNumberUnit.setNameUnit("");
        pfrTaxNumberUnit.setNumberUnit("");
        inventoryTerritorialAuthorityPFR.setAuthorityPFRTaxNumberUnit(pfrTaxNumberUnit);
        outputInventory.setAuthorityPFR(inventoryTerritorialAuthorityPFR);
        outputInventory.setNumberBank(hashMap.get("NumberBank"));
        OpvfOutputInventoryCompanyFormedDocuments companyFormedDocuments = new OpvfOutputInventoryCompanyFormedDocuments();
        companyFormedDocuments.setNameOfCompany(hashMap.get("Unit Company"));
        outputInventory.setCompanyFormedDocuments(companyFormedDocuments);
        outputInventory.setMonth(hashMap.get("Month"));
        outputInventory.setYear(hashMap.get("Year"));
        outputInventory.setAmountFilial(hashMap.get("AmountEnrolledFilial"));
        outputInventory.setRecipientEnrolled(hashMap.get("AmountRecipientEnrolled"));
        outputInventory.setNotAmountFilial(hashMap.get("AmountNotEnrolledFilial"));
        outputInventory.setRecipientNotEnrolled(hashMap.get("AmountRecipientNotEnrolled"));

        //int newOutputNumber = Integer.valueOf((hashMap.get("OutputNumber")));
        int tempNewOutputNumber = 0;
        if (tmpCountSpra>=1){
            tempNewOutputNumber = outputNumber + 2;
        } else {
            tempNewOutputNumber = outputNumber + 1;
        }

        String tmpNewOutputNumber = "";
        if (tempNewOutputNumber > 0 & tempNewOutputNumber <= 9) {
            tmpNewOutputNumber = "000000000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 9 & tempNewOutputNumber <= 99) {
            tmpNewOutputNumber = "00000000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 99 & tempNewOutputNumber <= 999) {
            tmpNewOutputNumber = "0000000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 999 & tempNewOutputNumber <= 9999) {
            tmpNewOutputNumber = "000000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 9999 & tempNewOutputNumber <= 99999) {
            tmpNewOutputNumber = "00000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 99999 & tempNewOutputNumber <= 999999) {
            tmpNewOutputNumber = "0000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 999999 & tempNewOutputNumber <= 9999999) {
            tmpNewOutputNumber = "000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 9999999 & tempNewOutputNumber <= 99999999) {
            tmpNewOutputNumber = "00" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 99999999 & tempNewOutputNumber <= 999999999) {
            tmpNewOutputNumber = "0" + tempNewOutputNumber;
        }

        outputInventory.setOuputNumber(tmpNewOutputNumber);
        outputInventory.setDateFormation(hashMap.get("DateFormation"));
        outputInventory.setPosition(hashMap.get("Position"));
        outputInventory.setHead(hashMap.get("Head"));


        opvfDocs.setOutputInventory(outputInventory);
        /******************************/

            /*Блок для ОписиВозвращаемых файлов*/
        OpvfInventoryReturnedFiles returnedFiles = new OpvfInventoryReturnedFiles();
        //returnedFiles.setNumberTransfferedFiles(ParsOppf.getOppf().getOppfInputDoc().getTransferredFiles().getNumberTransferredFiles());
        returnedFiles.setNumberTransfferedFiles(((listNameOutputFiles.size())+(listNameOutputFilesSpra.size())));
        ArrayList<OpvfInventoryReturnedFilesTransffered> returnedFilesTransffered = new ArrayList<>();

        for (OppfListTransferredFilesEnrollmentTrFiles x : ParsOppf.getOppf().getOppfInputDoc().getTransferredFiles().getTrFiles()) {
            OpvfInventoryReturnedFilesTransffered transfferedList = new OpvfInventoryReturnedFilesTransffered();
            transfferedList.setTypeDocument("ОТЧЕТ_О_ЗАЧИСЛЕНИИ_И_НЕ_ЗАЧИСЛЕНИИ_СУММ");
            transfferedList.setNameFile(listNameOutputFiles.get(count));
            transfferedList.setAmountRecipient(String.valueOf(x.getNumberRecipient())); //Количество получателей
            transfferedList.setNumberPartArray(String.valueOf(x.getPartNumberArray())); //Номер части массива
            transfferedList.setAmountPartArray(String.valueOf(x.getPartAmountArray())); //Сумма по массиву
            returnedFilesTransffered.add(transfferedList);
            count++;
        }

        if (tmpCountSpra >= 1){
            for (String x : listNameOutputFilesSpra){
                OpvfInventoryReturnedFilesTransffered transfferedList = new OpvfInventoryReturnedFilesTransffered();
                transfferedList.setTypeDocument("СПИСОК_РАСХОЖДЕНИЙ");
                transfferedList.setNameFile(x);
                returnedFilesTransffered.add(transfferedList);
            }
        }



        returnedFiles.setTransfferedFiles(returnedFilesTransffered);
        opvfDocs.setReturnedFiles(returnedFiles);
        opvf.setOpvfDocs(opvfDocs);
        /******************************/

        //int numberStack = ParsOppf.getOppf().getOppfInputDoc().getInputInventory().getNumberStack();
        //int mainNumberStack = ParsOppf.getOppf().getOppfInputDoc().getInputInventory().getInputInventoryTwinPack().getMain();

        String tmpMainNumberStack;
        String tmpNumberStack;

        if (numberStack > 0 & numberStack <= 9) {
            tmpNumberStack = "00" + numberStack;
        } else if (numberStack > 9 & numberStack <= 99) {
            tmpNumberStack = "0" + numberStack;
        } else {
            tmpNumberStack = String.valueOf(numberStack);
        }

        if (mainNumberStack > 0 & mainNumberStack <= 9) {
            tmpMainNumberStack = "0000" + mainNumberStack;
        } else if (mainNumberStack > 9 & mainNumberStack <= 99) {
            tmpMainNumberStack = "000" + mainNumberStack;
        } else if (mainNumberStack > 99 & mainNumberStack <= 999) {
            tmpMainNumberStack = "00" + mainNumberStack;
        } else if (mainNumberStack > 999 & mainNumberStack <= 9999) {
            tmpMainNumberStack = "0" + mainNumberStack;
        } else {
            tmpMainNumberStack = String.valueOf(mainNumberStack);
        }

        String nameFile = "OUT-700-Y-" + hashMap.get("Year") + "-ORG-" + hashMap.get("AuthorityPfrRegistrationNumber")
                + "-DIS-038-DCK-" + tmpMainNumberStack + "-" + tmpNumberStack + "-DOC-OPVF-FSB-"
                + hashMap.get("NumberBank") + "-OUTNMB-" + tmpNewOutputNumber + ".XML";
        //String nameFile = ;
        //listNameOutputFiles.add(nameFile);
        /*Блок для имени файла*/
        opvf.setNameFile(nameFile);
        /*****************************/


        try {
            File file = new File(outputPath + "\\" + nameFile);
            JAXBContext jc = JAXBContext.newInstance(Opvf.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(opvf, file);
            new PosdBase(outputPath, nameFileOzac, nameFile, hashMap, Integer.valueOf(tmpNewOutputNumber), listNameInputFiles, numberStack, mainNumberStack);
            //marshaller.marshal(opvf, System.out);
        } catch (Exception e) {
        }

    }
}
