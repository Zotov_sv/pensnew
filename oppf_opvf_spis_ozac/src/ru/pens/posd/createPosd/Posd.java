package ru.pens.posd.createPosd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Admin on 08.02.2017.
 */
@XmlRootElement(name = "ФайлПФР")
@XmlAccessorType(XmlAccessType.FIELD)
public class Posd {

    @XmlElement(name = "ИмяФайла")
    private String nameFile;

    @XmlElement(name = "ЗаголовокФайла")
    private PosdHeaderFile headerFile;

    @XmlElement(name = "ПачкаИсходящихДокументов")
    private PosdDocs posdDocs;

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public void setHeaderFile(PosdHeaderFile headerFile) {
        this.headerFile = headerFile;
    }

    public void setPosdDocs(PosdDocs posdDocs) {
        this.posdDocs = posdDocs;
    }
}
