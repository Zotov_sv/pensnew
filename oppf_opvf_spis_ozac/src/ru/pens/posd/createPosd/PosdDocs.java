package ru.pens.posd.createPosd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 08.02.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class PosdDocs {

    @XmlAttribute(name = "ДоставочнаяОрганизация")
    private String bank = "Банк";

    @XmlElement(name = "ИСХОДЯЩАЯ_ОПИСЬ")
    private PosdOutputInventory outputInventory;

    @XmlElement(name = "ПОДТВЕРЖДЕНИЕ_О_ПРОЧТЕНИИ_СПИСКОВ_ПРИ_ЗАЧИСЛЕНИИ")
    private PosdInventoryReturnedFiles returnedFiles;

    public void setBank(String bank) {
        this.bank = bank;
    }

    public void setOutputInventory(PosdOutputInventory outputInventory) {
        this.outputInventory = outputInventory;
    }

    public void setReturnedFiles(PosdInventoryReturnedFiles returnedFiles) {
        this.returnedFiles = returnedFiles;
    }

}
