package ru.pens.posd.createPosd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 08.02.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PosdHeaderFileTrainingProgramData {

    @XmlElement(name = "НазваниеПрограммы")
    private String nameOfMyProgram;

    @XmlElement(name = "Версия")
    private String version;

    public void setNameOfMyProgram(String nameOfMyProgram) {
        this.nameOfMyProgram = nameOfMyProgram;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
