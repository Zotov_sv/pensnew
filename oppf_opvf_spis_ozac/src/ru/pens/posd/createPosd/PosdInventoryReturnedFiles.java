package ru.pens.posd.createPosd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by Admin on 10.02.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PosdInventoryReturnedFiles {

    @XmlElement(name = "Количество")
    private int quantity;

    @XmlElement(name = "СведенияОмассивеПоручений")
    private List<PosdInventoryReturnedFilesInformationArraysOrder> informationArraysOrder;

    @XmlElement(name = "Решение")
    private String resultInventory;

    @XmlElement(name = "ДатаВыдачиДокумента")
    private String date;

    @XmlElement(name = "ВремяФормирования")
    private String time;

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setInformationArraysOrder(List<PosdInventoryReturnedFilesInformationArraysOrder> informationArraysOrder) {
        this.informationArraysOrder = informationArraysOrder;
    }

    public void setResultInventory(String resultInventory) {
        this.resultInventory = resultInventory;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
