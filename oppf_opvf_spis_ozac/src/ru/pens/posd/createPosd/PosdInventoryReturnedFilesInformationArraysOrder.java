package ru.pens.posd.createPosd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 10.02.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class PosdInventoryReturnedFilesInformationArraysOrder {

    @XmlElement(name = "ТипСтроки")
    private String stringType;

    @XmlElement(name = "ИмяФайла")
    private String fileName;

    @XmlElement(name = "ПодтверждениеОПрочтении")
    private String readConfirmation;

    @XmlElement(name = "РезультатПроверки")
    private String resultCheck;

    @XmlElement(name = "КоличествоПоступившихФайлов")
    private String numberReceivedFiles;

    @XmlElement(name = "СистемныйНомерМассива")
    private String systemNumberArray;

    public void setStringType(String stringType) {
        this.stringType = stringType;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setSystemNumberArray(String systemNumberArray) {
        this.systemNumberArray = systemNumberArray;
    }

    public void setReadConfirmation(String readConfirmation) {
        this.readConfirmation = readConfirmation;
    }

    public void setResultCheck(String resultCheck) {
        this.resultCheck = resultCheck;
    }

    public void setNumberReceivedFiles(String  numberReceivedFiles) {
        this.numberReceivedFiles = numberReceivedFiles;
    }
}

