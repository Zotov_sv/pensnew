package ru.pens.posd.createPosd;

import ru.pens.oppf_opvf.createOpvf.OpvfOutputInventoryCompanyFormedDocuments;
import ru.pens.oppf_opvf.createOpvf.OpvfOutputInventoryCompiledPacks;
import ru.pens.oppf_opvf.createOpvf.OpvfOutputInventoryTerritorialAuthorityPFR;
import ru.pens.oppf_opvf.createOpvf.OpvfOutputInventoryTheDocuments;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 10.02.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class PosdOutputInventory {

        @XmlElement(name = "СоставительПачки")
        private PosdOutputInventoryCompiledPacks compiliedPacks;

        @XmlElement (name = "СоставДокументов")
        private PosdOutputInventoryTheDocuments posdOutputInventoryTheDocuments;

        @XmlElement (name = "ТерриториальныйОрганПФР")
        private PosdOutputInventoryTerritorialAuthorityPFR authorityPFR;

        @XmlElement (name = "НомерБанка")
        private String numberBank;

        @XmlElement (name = "ОрганизацияСформировавшаяДокумент")
        private PosdOutputInventoryCompanyFormedDocuments companyFormedDocuments;

        @XmlElement (name = "ТипМассиваПоручений")
        private String signArraysOrder;

        @XmlElement (name = "Месяц")
        private String month;

        @XmlElement (name = "Год")
        private String year;

        @XmlElement (name = "ИсходящийНомер")
        private String ouputNumber;

        @XmlElement (name = "ДатаФормирования")
        private String dateFormation;

        @XmlElement (name = "Должность")
        private String position;

        @XmlElement (name = "Руководитель")
        private String head;

    public void setCompiliedPacks(PosdOutputInventoryCompiledPacks compiliedPacks) {
        this.compiliedPacks = compiliedPacks;
    }

    public void setPosdOutputInventoryTheDocuments(PosdOutputInventoryTheDocuments posdOutputInventoryTheDocuments) {
        this.posdOutputInventoryTheDocuments = posdOutputInventoryTheDocuments;
    }

    public void setAuthorityPFR(PosdOutputInventoryTerritorialAuthorityPFR authorityPFR) {
        this.authorityPFR = authorityPFR;
    }

    public void setNumberBank(String numberBank) {
        this.numberBank = numberBank;
    }

    public void setCompanyFormedDocuments(PosdOutputInventoryCompanyFormedDocuments companyFormedDocuments) {
        this.companyFormedDocuments = companyFormedDocuments;
    }

    public void setSignArraysOrder(String signArraysOrder) {
        this.signArraysOrder = signArraysOrder;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setOuputNumber(String ouputNumber) {
        this.ouputNumber = ouputNumber;
    }

    public void setDateFormation(String dateFormation) {
        this.dateFormation = dateFormation;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setHead(String head) {
        this.head = head;
    }
}
