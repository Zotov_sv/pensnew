package ru.pens.posd.createPosd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 02.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PosdOutputInventoryCompanyFormedDocuments {

        @XmlElement(name = "НаименованиеОрганизации")
        private String nameOfCompany;

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }
}
