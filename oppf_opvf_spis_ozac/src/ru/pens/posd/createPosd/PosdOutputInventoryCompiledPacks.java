package ru.pens.posd.createPosd;

import ru.pens.oppf_opvf.createOpvf.OpvfOutputInventoryCompiledPacksTaxNumber;
import ru.pens.oppf_opvf.createOpvf.OpvfOutputInventoryCompiledPacksUnit;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 02.03.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class PosdOutputInventoryCompiledPacks {


    @XmlElement(name = "НалоговыйНомер")
    private PosdOutputInventoryCompiledPacksTaxNumber packsTaxNumber;

    @XmlElement (name = "НаименованиеОрганизации")
    private String nameOfCompany;

    @XmlElement(name = "РегистрационныйНомер")
    private String  packsRegistrationNumber;

    @XmlElement(name = "Подразделение")
    private PosdOutputInventoryCompiledPacksUnit compiledPacksUnit;

    public void setPacksTaxNumber(PosdOutputInventoryCompiledPacksTaxNumber packsTaxNumber) {
        this.packsTaxNumber = packsTaxNumber;
    }

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }

    public void setPacksRegistrationNumber(String packsRegistrationNumber) {
        this.packsRegistrationNumber = packsRegistrationNumber;
    }

    public void setCompiledPacksUnit(PosdOutputInventoryCompiledPacksUnit compiledPacksUnit) {
        this.compiledPacksUnit = compiledPacksUnit;
    }
}
