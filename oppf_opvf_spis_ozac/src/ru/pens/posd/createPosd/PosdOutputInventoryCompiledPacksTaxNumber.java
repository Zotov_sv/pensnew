package ru.pens.posd.createPosd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 02.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PosdOutputInventoryCompiledPacksTaxNumber {

        @XmlElement(name = "ИНН")
        private String inn;

        @XmlElement (name = "КПП")
        private String kpp;

    public void setInn(String inn) {
        this.inn = inn;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }
}
