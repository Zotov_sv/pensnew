package ru.pens.posd.createPosd;

import ru.pens.oppf_opvf.createOpvf.OpvfOutputInventoryTerritorialAuthorityPFRTaxNumber;
import ru.pens.oppf_opvf.createOpvf.OpvfOutputInventoryTerritorialAuthorityPFRTaxNumberUnit;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 02.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PosdOutputInventoryTerritorialAuthorityPFR {




        @XmlElement(name = "НалоговыйНомер")
        private PosdOutputInventoryTerritorialAuthorityPFRTaxNumber pfrTaxNumber;

        @XmlElement(name = "НаименованиеОрганизации")
        private String nameOfCompany;

        @XmlElement(name = "РегистрационныйНомер")
        private String registrationNumber;

        @XmlElement(name = "Подразделение")
        private PosdOutputInventoryTerritorialAuthorityPFRTaxNumberUnit authorityPFRTaxNumberUnit;

    public void setPfrTaxNumber(PosdOutputInventoryTerritorialAuthorityPFRTaxNumber pfrTaxNumber) {
        this.pfrTaxNumber = pfrTaxNumber;
    }

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public void setAuthorityPFRTaxNumberUnit(PosdOutputInventoryTerritorialAuthorityPFRTaxNumberUnit authorityPFRTaxNumberUnit) {
        this.authorityPFRTaxNumberUnit = authorityPFRTaxNumberUnit;
    }
}
