package ru.pens.posd.createPosd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 02.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PosdOutputInventoryTerritorialAuthorityPFRTaxNumberUnit {

        @XmlElement(name = "НаименованиеПодразделения")
        private String nameUnit;

        @XmlElement (name = "НомерПодразделения")
        private String numberUnit;

    public void setNameUnit(String nameUnit) {
        this.nameUnit = nameUnit;
    }

    public void setNumberUnit(String numberUnit) {
        this.numberUnit = numberUnit;
    }
}
