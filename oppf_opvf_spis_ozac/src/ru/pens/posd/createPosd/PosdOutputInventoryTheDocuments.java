package ru.pens.posd.createPosd;

import ru.pens.oppf_opvf.createOpvf.OpvfOutputInventoryTheDocumentsAvailabilityOfDocuments;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 02.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PosdOutputInventoryTheDocuments {

        @XmlElement(name = "Количество")
        private int quantity;

        @XmlElement(name = "НаличиеДокументов")
        private PosdOutputInventoryTheDocumentsAvailabilityOfDocuments availabilityOfDocuments;

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setAvailabilityOfDocuments(PosdOutputInventoryTheDocumentsAvailabilityOfDocuments availabilityOfDocuments) {
        this.availabilityOfDocuments = availabilityOfDocuments;
    }
}
