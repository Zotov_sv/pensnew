package ru.pens.posd.createPosd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 02.03.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PosdOutputInventoryTheDocumentsAvailabilityOfDocuments {

        @XmlElement(name = "ТипДокумента")
        private String signDocument;

        @XmlElement(name = "Количество")
        private int quantity;

    public void setSignDocument(String signDocument) {
        this.signDocument = signDocument;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
