package ru.pens.posd.withFiles;



import ru.pens.global.counter.AllCounter;
import ru.pens.oppf_opvf.dataProcessing.ParsOppf;
import ru.pens.posd.createPosd.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 15.02.2017.
 */



public class PosdBase {

    Posd posd = new Posd();
    private ArrayList<PosdInventoryReturnedFilesInformationArraysOrder> listInventoryReturnedFilesInformationArraysOrders = new ArrayList<>();

    public PosdBase(String outputPath, String nameFileOzac, String nameFileOpvf, HashMap<String,
            String> hashMap, int opvfOutputNumber, ArrayList<String> listNameInputFiles,int numberStack,int mainNumberStack) {




        /*Блок для заголовка файла*/
        PosdHeaderFile headerFile = new PosdHeaderFile();
        headerFile.setVersionFormat("07.00");
        headerFile.setTypeFile("ВНЕШНИЙ");
            PosdHeaderFileTrainingProgramData trainingProgramData = new PosdHeaderFileTrainingProgramData();
            trainingProgramData.setNameOfMyProgram("МосуралАйтиПродакшн");
            trainingProgramData.setVersion("ВЕРСИЯ1.12");
        headerFile.setProgramData(trainingProgramData);
        headerFile.setDataSource("ДОСТАВЩИК");
        /****************/

        /*Блок для исходящих документов*/
        PosdDocs posdDocs = new PosdDocs();

        /*Блок для ИсходящейОписи*/
        PosdOutputInventory outputInventory = new PosdOutputInventory();
            PosdOutputInventoryCompiledPacks compiledPacks = new PosdOutputInventoryCompiledPacks();
                PosdOutputInventoryCompiledPacksTaxNumber compiledPacksTaxNumber = new PosdOutputInventoryCompiledPacksTaxNumber();
                //compiledPacksTaxNumber.setInn("7707083011");
                //compiledPacksTaxNumber.setKpp("662343001");
                compiledPacksTaxNumber.setInn(hashMap.get("INN"));
                compiledPacksTaxNumber.setKpp(hashMap.get("KPP"));
            compiledPacks.setPacksTaxNumber(compiledPacksTaxNumber);
            compiledPacks.setNameOfCompany(hashMap.get("NameCompany"));
            compiledPacks.setPacksRegistrationNumber(hashMap.get("Registration Number"));
                PosdOutputInventoryCompiledPacksUnit compiledPacksUnit = new PosdOutputInventoryCompiledPacksUnit();
                compiledPacksUnit.setNameUnit(hashMap.get("Unit Company"));
                compiledPacksUnit.setNumberUnit(hashMap.get("Unit RegistrationNumber"));
            compiledPacks.setCompiledPacksUnit(compiledPacksUnit);
        outputInventory.setCompiliedPacks(compiledPacks);

            PosdOutputInventoryTheDocuments inventoryTheDocuments = new PosdOutputInventoryTheDocuments();
            //Вопрос че за вата.
            inventoryTheDocuments.setQuantity(ParsOppf.getOppf().getOppfInputDoc().getInputInventory().getInputInventoryDocumentCompositionList().iterator().next().getQuantity());
                PosdOutputInventoryTheDocumentsAvailabilityOfDocuments availabilityOfDocuments = new PosdOutputInventoryTheDocumentsAvailabilityOfDocuments();
                //Такая же херь
                availabilityOfDocuments.setQuantity(ParsOppf.getOppf().getOppfInputDoc().getInputInventory().getInputInventoryDocumentCompositionList().
                        iterator().next().getQuantity());
                availabilityOfDocuments.setSignDocument("ПОДТВЕРЖДЕНИЕ_О_ПРОЧТЕНИИ_СПИСКОВ_ПРИ_ЗАЧИСЛЕНИИ");
            inventoryTheDocuments.setAvailabilityOfDocuments(availabilityOfDocuments);
        outputInventory.setPosdOutputInventoryTheDocuments(inventoryTheDocuments);

            PosdOutputInventoryTerritorialAuthorityPFR inventoryTerritorialAuthorityPFR = new PosdOutputInventoryTerritorialAuthorityPFR();
                PosdOutputInventoryTerritorialAuthorityPFRTaxNumber authorityPFRTaxNumber = new PosdOutputInventoryTerritorialAuthorityPFRTaxNumber();
                authorityPFRTaxNumber.setInn(hashMap.get("AuthorityPfrInn"));
                authorityPFRTaxNumber.setKpp(hashMap.get("AuthorityPfrKpp"));
            inventoryTerritorialAuthorityPFR.setPfrTaxNumber(authorityPFRTaxNumber);
            inventoryTerritorialAuthorityPFR.setNameOfCompany(hashMap.get("AuthorityPfrNameCompany"));
            inventoryTerritorialAuthorityPFR.setRegistrationNumber(hashMap.get("AuthorityPfrRegistrationNumber"));
        outputInventory.setAuthorityPFR(inventoryTerritorialAuthorityPFR);
        outputInventory.setNumberBank(hashMap.get("NumberBank"));
            PosdOutputInventoryCompanyFormedDocuments companyFormedDocuments = new PosdOutputInventoryCompanyFormedDocuments();
            companyFormedDocuments.setNameOfCompany(hashMap.get("Unit Company"));
        outputInventory.setCompanyFormedDocuments(companyFormedDocuments);
        outputInventory.setMonth(hashMap.get("Month"));
        outputInventory.setYear(hashMap.get("Year"));




        //int newOutputNumber = (opvfOutputNumber+listNameInputFiles.size());
        int newOutputNumber = (opvfOutputNumber+1);
        String tmpNewOutputNumber = "";
        if (newOutputNumber > 0 & newOutputNumber <= 9) {
            tmpNewOutputNumber = "000000000" + newOutputNumber;
        } else if (newOutputNumber > 9 & newOutputNumber <= 99) {
            tmpNewOutputNumber = "00000000" + newOutputNumber;
        } else if (newOutputNumber > 99 & newOutputNumber <= 999) {
            tmpNewOutputNumber = "0000000" + newOutputNumber;
        } else if (newOutputNumber > 999 & newOutputNumber <= 9999) {
            tmpNewOutputNumber = "000000" + newOutputNumber;
        } else if (newOutputNumber > 9999 & newOutputNumber <= 99999) {
            tmpNewOutputNumber = "00000" + newOutputNumber;
        } else if (newOutputNumber > 99999 & newOutputNumber <= 999999) {
            tmpNewOutputNumber = "0000" + newOutputNumber;
        } else if (newOutputNumber > 999999 & newOutputNumber <= 9999999) {
            tmpNewOutputNumber = "000" + newOutputNumber;
        } else if (newOutputNumber > 9999999 & newOutputNumber <= 99999999) {
            tmpNewOutputNumber = "00" + newOutputNumber;
        } else if (newOutputNumber > 99999999 & newOutputNumber <= 999999999) {
            tmpNewOutputNumber = "0" + newOutputNumber;
        }
        outputInventory.setOuputNumber(tmpNewOutputNumber);
        outputInventory.setDateFormation(hashMap.get("DateFormation"));
        outputInventory.setPosition(hashMap.get("Position"));
        outputInventory.setHead(hashMap.get("Head"));



        /*Подтверждение о прочтении списков при зачислении*/
            PosdInventoryReturnedFiles posdInventoryReturnedFiles = new PosdInventoryReturnedFiles();
            posdInventoryReturnedFiles.setQuantity(listNameInputFiles.size());
                for (String x : listNameInputFiles){
                    PosdInventoryReturnedFilesInformationArraysOrder arraysOrder = new PosdInventoryReturnedFilesInformationArraysOrder();
                    arraysOrder.setStringType("ДЕТАЛЬНАЯ");
                    arraysOrder.setFileName(x);
                    arraysOrder.setSystemNumberArray(hashMap.get("SystemNumberArray"));
                    arraysOrder.setReadConfirmation("ПРОЧТЕНО");
                    arraysOrder.setResultCheck("ПРАВИЛЬНО");
                    listInventoryReturnedFilesInformationArraysOrders.add(arraysOrder);
                }
                PosdInventoryReturnedFilesInformationArraysOrder arraysOrder = new PosdInventoryReturnedFilesInformationArraysOrder();
                    arraysOrder.setStringType("ИТОГО");
                    arraysOrder.setNumberReceivedFiles(String.valueOf(listNameInputFiles.size()));
                    arraysOrder.setSystemNumberArray(hashMap.get("SystemNumberArray"));

                    listInventoryReturnedFilesInformationArraysOrders.add(arraysOrder);
            posdInventoryReturnedFiles.setInformationArraysOrder(listInventoryReturnedFilesInformationArraysOrders);
            posdInventoryReturnedFiles.setResultInventory("ПРИНЯТО");
            posdInventoryReturnedFiles.setDate(hashMap.get("DateFormation"));
            posdInventoryReturnedFiles.setTime(hashMap.get("TimeFormation"));
        /***************/

        posdDocs.setOutputInventory(outputInventory);
        posdDocs.setReturnedFiles(posdInventoryReturnedFiles);

        String tmpMainNumberStack;
        String tmpNumberStack;

        if (numberStack > 0 & numberStack <= 9) {
            tmpNumberStack = "00" + numberStack;
        } else if (numberStack > 9 & numberStack <= 99) {
            tmpNumberStack = "0" + numberStack;
        } else {
            tmpNumberStack = String.valueOf(numberStack);
        }

        if (mainNumberStack > 0 & mainNumberStack <= 9) {
            tmpMainNumberStack = "0000" + mainNumberStack;
        } else if (mainNumberStack > 9 & mainNumberStack <= 99) {
            tmpMainNumberStack = "000" + mainNumberStack;
        } else if (mainNumberStack > 99 & mainNumberStack <= 999) {
            tmpMainNumberStack = "00" + mainNumberStack;
        } else if (mainNumberStack > 999 & mainNumberStack <= 9999) {
            tmpMainNumberStack = "0" + mainNumberStack;
        } else {
            tmpMainNumberStack = String.valueOf(mainNumberStack);
        }



   /*Блок для имени файла*/
        String nameFile = "OUT-700-Y-" + hashMap.get("Year") + "-ORG-" + hashMap.get("AuthorityPfrRegistrationNumber")
                + "-DIS-038-DCK-" + tmpMainNumberStack + "-" + tmpNumberStack + "-DOC-POSD-FSB-"
                + hashMap.get("NumberBank") + "-OUTNMB-" + tmpNewOutputNumber + ".XML";

        /**************/


        posd.setNameFile(nameFile);
        posd.setHeaderFile(headerFile);
        posd.setPosdDocs(posdDocs);

        try {
            File file = new File(outputPath + "\\" + nameFile);
            JAXBContext jc = JAXBContext.newInstance(Posd.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
            marshaller.marshal(posd, file);

            AllCounter.allCounter().recordCount(newOutputNumber);

            //marshaller.marshal(posd, System.out);
        } catch (Exception e) {

        }
    }
}
