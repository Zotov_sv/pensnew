package ru.pens.spis_ozac.createOzac;

import ru.pens.spis_ozac.parsSpis.SpisHeaderFile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Admin on 14.12.2016.
 */
@XmlRootElement(name = "ФайлПФР")
@XmlAccessorType(XmlAccessType.FIELD)
public class Ozac {

    @XmlElement (name = "ИмяФайла")
    private String nameFile;

    @XmlElement (name = "ЗаголовокФайла")
    private SpisHeaderFile spisHeaderFile;

    @XmlElement(name = "ПачкаИсходящихДокументов")
    private OzacDocs ozacDocs;

    public void setOzacDocs(OzacDocs ozacDocs) {
        this.ozacDocs = ozacDocs;
    }

    public String getNameFile() {
        return nameFile;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public SpisHeaderFile getSpisHeaderFile() {
        return spisHeaderFile;
    }

    public void setSpisHeaderFile(SpisHeaderFile spisHeaderFile) {
        this.spisHeaderFile = spisHeaderFile;
    }

    public OzacDocs getOzacDocs() {
        return ozacDocs;
    }
}
