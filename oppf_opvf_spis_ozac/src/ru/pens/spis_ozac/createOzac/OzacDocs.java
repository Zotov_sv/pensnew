package ru.pens.spis_ozac.createOzac;

import ru.pens.spis_ozac.parsSpis.SpisInputInventory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


/**
 * Created by Admin on 14.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OzacDocs {

    @XmlAttribute(name = "ДоставочнаяОрганизация")
    private String bank = "Банк";

    @XmlElement (name = "ВХОДЯЩАЯ_ОПИСЬ")
    private SpisInputInventory createXmlSpisInputInventory;

    @XmlElement (name = "ИСХОДЯЩАЯ_ОПИСЬ")
    private OzacOutputInventory ozacOutputInventory;

    @XmlElement(name = "ОТЧЕТ_О_ЗАЧИСЛЕНИИ_И_НЕ_ЗАЧИСЛЕНИИ_СУММ")
    private OzacElements ozacElements;

    public SpisInputInventory getCreateXmlSpisInputInventory() {
        return createXmlSpisInputInventory;
    }

    public OzacOutputInventory getOzacOutputInventory() {
        return ozacOutputInventory;
    }

    public OzacElements getOzacElements() {
        return ozacElements;
    }

    public void setOzacElements(OzacElements ozacElements) {
        this.ozacElements = ozacElements;
    }

    public void setCreateXmlSpisInputInventory(SpisInputInventory createXmlSpisInputInventory) {
        this.createXmlSpisInputInventory = createXmlSpisInputInventory;
    }

    public void setOzacOutputInventory(OzacOutputInventory ozacOutputInventory) {
        this.ozacOutputInventory = ozacOutputInventory;
    }
}
