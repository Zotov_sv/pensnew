package ru.pens.spis_ozac.createOzac;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;

/**
 * Created by Admin on 13.12.2016.
 */
//@XmlRootElement (name="ОТЧЕТ_О_ЗАЧИСЛЕНИИ_И_НЕ_ЗАЧИСЛЕНИИ_СУММ")
@XmlAccessorType(XmlAccessType.FIELD)
public class OzacElements {

    @XmlElement(name = "ОтчетПоПолучателю")
    private ArrayList<OzacElementsDetails> reportRecipient;

    public void setReportRecipient(ArrayList<OzacElementsDetails> reportRecipient) {
        this.reportRecipient = reportRecipient;
    }
}
