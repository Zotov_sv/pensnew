package ru.pens.spis_ozac.createOzac;

import ru.pens.spis_ozac.parsSpis.SpisListPayDetailsRecipientFio;
import ru.pens.spis_ozac.parsSpis.SpisListPayDetailsRecipientAllPay;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by Admin on 13.12.2016.
 */


@XmlAccessorType(XmlAccessType.FIELD)
public class OzacElementsDetails {

    @XmlElement(name = "НомерВмассиве")
    private int idArray;
    @XmlElement(name = "НомерВыплатногоДела")
    private String numberPaymasterDeal;
    @XmlElement(name = "КодРайона")
    private String areaCode;
    @XmlElement(name = "СтраховойНомер")
    private String insuranceNumber;
    @XmlElement(name = "ФИО")
    private List<SpisListPayDetailsRecipientFio> nameSpisListPayDetailsRecipientFio;
    @XmlElement(name = "НомерСчета")
    private String bill;
    @XmlElement(name = "ВсеВыплаты")
    private List<SpisListPayDetailsRecipientAllPay> pays;
    @XmlElement(name = "СуммаКдоставке")
    private double deliveredSum;
    @XmlElement(name = "КодЗачисления")
    private String enrollmentCode;
    @XmlElement(name = "ДатаФактическойДоставки")
    private String actualDeliveryDate;
    @XmlElement(name = "ДатаВыдачиДокумента")
    private String today;


    public void setIdArray(int idArray) {
        this.idArray = idArray;
    }

    public void setNumberPaymasterDeal(String numberPaymasterDeal) {
        this.numberPaymasterDeal = numberPaymasterDeal;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public void setNameSpisListPayDetailsRecipientFio(List<SpisListPayDetailsRecipientFio> nameSpisListPayDetailsRecipientFio) {
        this.nameSpisListPayDetailsRecipientFio = nameSpisListPayDetailsRecipientFio;
    }

    public void setBill(String bill) {
        this.bill = bill;
    }

    public void setPays(List<SpisListPayDetailsRecipientAllPay> pays) {
        this.pays = pays;
    }

    public void setDeliveredSum(double deliveredSum) {
        this.deliveredSum = deliveredSum;
    }

    public void setEnrollmentCode(String enrollmentCode) {
        this.enrollmentCode = enrollmentCode;
    }

    public void setActualDeliveryDate(String actualDeliveryDate) {
        this.actualDeliveryDate = actualDeliveryDate;
    }

    public void setToday(String today) {
        this.today = today;
    }
}
