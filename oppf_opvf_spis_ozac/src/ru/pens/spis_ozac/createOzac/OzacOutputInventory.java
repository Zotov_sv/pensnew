package ru.pens.spis_ozac.createOzac;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 15.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OzacOutputInventory {

    @XmlElement (name = "СоставительПачки")
    private OzacOutputInventoryCompiledPacks compiliedPacks;

    @XmlElement (name = "СоставДокументов")
    private OzacOutputInventoryTheDocuments ozacOutputInventoryTheDocuments;

    @XmlElement (name = "ТерриториальныйОрганПФР")
    private OzacOutputInventoryTerritorialAuthorityPFR authorityPFR;

    @XmlElement (name = "НомерБанка")
    private String numberBank;

    @XmlElement (name = "ОрганизацияСформировавшаяДокумент")
    private OzacOutputInventoryCompanyFormedDocuments companyFormedDocuments;

    @XmlElement (name = "НомерПлатежногоПоручения")
    private String numberPaymentOrder;

    @XmlElement (name = "ДатаПлатежногоПоручения")
    private String datePaymentOrder;

    @XmlElement (name = "СуммаПоПлатежномуПоручению")
    private String amountPaymentOrder;

    @XmlElement (name = "ТипМассиваПоручений")
    private String signArraysOrder;

    @XmlElement (name = "Месяц")
    private String month;

    @XmlElement (name = "Год")
    private String year;

    @XmlElement (name = "СуммаЗачисленоПоФилиалу")
    private String amountFilial;

    @XmlElement (name = "КоличествоПолучателейЗачислено")
    private String recipientEnrolled;

    @XmlElement (name = "СуммаНеЗачисленоПоФилиалу")
    private String notAmountFilial;

    @XmlElement (name = "КоличествоПолучателейНеЗачислено")
    private String recipientNotEnrolled;

    @XmlElement (name = "ИсходящийНомер")
    private String outputNumber;

    @XmlElement (name = "ДатаФормирования")
    private String dateFormation;

    @XmlElement (name = "Должность")
    private String position;

    @XmlElement (name = "Руководитель")
    private String head;

    public OzacOutputInventoryCompiledPacks getCompiliedPacks() {
        return compiliedPacks;
    }

    public void setCompiliedPacks(OzacOutputInventoryCompiledPacks compiliedPacks) {
        this.compiliedPacks = compiliedPacks;
    }

    public OzacOutputInventoryTheDocuments getOzacOutputInventoryTheDocuments() {
        return ozacOutputInventoryTheDocuments;
    }

    public void setOzacOutputInventoryTheDocuments(OzacOutputInventoryTheDocuments ozacOutputInventoryTheDocuments) {
        this.ozacOutputInventoryTheDocuments = ozacOutputInventoryTheDocuments;
    }

    public OzacOutputInventoryTerritorialAuthorityPFR getAuthorityPFR() {
        return authorityPFR;
    }

    public void setAuthorityPFR(OzacOutputInventoryTerritorialAuthorityPFR authorityPFR) {
        this.authorityPFR = authorityPFR;
    }

    public String getNumberBank() {
        return numberBank;
    }

    public void setNumberBank(String numberBank) {
        this.numberBank = numberBank;
    }

    public OzacOutputInventoryCompanyFormedDocuments getCompanyFormedDocuments() {
        return companyFormedDocuments;
    }

    public void setCompanyFormedDocuments(OzacOutputInventoryCompanyFormedDocuments companyFormedDocuments) {
        this.companyFormedDocuments = companyFormedDocuments;
    }

    public String getNumberPaymentOrder() {
        return numberPaymentOrder;
    }

    public void setNumberPaymentOrder(String numberPaymentOrder) {
        this.numberPaymentOrder = numberPaymentOrder;
    }

    public String getDatePaymentOrder() {
        return datePaymentOrder;
    }

    public void setDatePaymentOrder(String datePaymentOrder) {
        this.datePaymentOrder = datePaymentOrder;
    }

    public String getAmountPaymentOrder() {
        return amountPaymentOrder;
    }

    public void setAmountPaymentOrder(String amountPaymentOrder) {
        this.amountPaymentOrder = amountPaymentOrder;
    }

    public String getSignArraysOrder() {
        return signArraysOrder;
    }

    public void setSignArraysOrder(String signArraysOrder) {
        this.signArraysOrder = signArraysOrder;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getAmountFilial() {
        return amountFilial;
    }

    public void setAmountFilial(String amountFilial) {
        this.amountFilial = amountFilial;
    }

    public String getRecipientEnrolled() {
        return recipientEnrolled;
    }

    public void setRecipientEnrolled(String recipientEnrolled) {
        this.recipientEnrolled = recipientEnrolled;
    }

    public String getNotAmountFilial() {
        return notAmountFilial;
    }

    public void setNotAmountFilial(String notAmountFilial) {
        this.notAmountFilial = notAmountFilial;
    }

    public String getRecipientNotEnrolled() {
        return recipientNotEnrolled;
    }

    public void setRecipientNotEnrolled(String recipientNotEnrolled) {
        this.recipientNotEnrolled = recipientNotEnrolled;
    }

    public String getOutputNumber() {
        return outputNumber;
    }

    public void setOutputNumber(String outputNumber) {
        this.outputNumber = outputNumber;
    }

    public String getDateFormation() {
        return dateFormation;
    }

    public void setDateFormation(String dateFormation) {
        this.dateFormation = dateFormation;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }
}
