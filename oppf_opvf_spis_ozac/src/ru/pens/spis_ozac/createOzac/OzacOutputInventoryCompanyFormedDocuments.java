package ru.pens.spis_ozac.createOzac;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by I on 16.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OzacOutputInventoryCompanyFormedDocuments {

    @XmlElement (name = "НаименованиеОрганизации")
    private String nameOfCompany;

    public String getNameOfCompany() {
        return nameOfCompany;
    }

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }
}
