package ru.pens.spis_ozac.createOzac;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by I on 16.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OzacOutputInventoryCompiledPacks {

    @XmlElement(name = "НалоговыйНомер")
    private OzacOutputInventoryCompiledPacksTaxNumber packsTaxNumber;

    @XmlElement (name = "НаименованиеОрганизации")
    private String nameOfCompany;

    @XmlElement(name = "РегистрационныйНомер")
    private String packsRegistrationNumber;

    @XmlElement(name = "Подразделение")
    private OzacOutputInventoryCompiledPacksUnit compiledPacksUnit;

    public String getNameOfCompany() {
        return nameOfCompany;
    }

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }

    public OzacOutputInventoryCompiledPacksTaxNumber getPacksTaxNumber() {
        return packsTaxNumber;
    }

    public void setPacksTaxNumber(OzacOutputInventoryCompiledPacksTaxNumber packsTaxNumber) {
        this.packsTaxNumber = packsTaxNumber;
    }

    public String getPacksRegistrationNumber() {
        return packsRegistrationNumber;
    }

    public void setPacksRegistrationNumber(String packsRegistrationNumber) {
        this.packsRegistrationNumber = packsRegistrationNumber;
    }

    public OzacOutputInventoryCompiledPacksUnit getCompiledPacksUnit() {
        return compiledPacksUnit;
    }

    public void setCompiledPacksUnit(OzacOutputInventoryCompiledPacksUnit compiledPacksUnit) {
        this.compiledPacksUnit = compiledPacksUnit;
    }
}
