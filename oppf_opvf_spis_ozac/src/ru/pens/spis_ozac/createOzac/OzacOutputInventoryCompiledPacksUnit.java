package ru.pens.spis_ozac.createOzac;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 27.01.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OzacOutputInventoryCompiledPacksUnit {

    @XmlElement (name = "НаименованиеПодразделения")
    private String nameUnit;

    @XmlElement (name = "НомерПодразделения")
    private String numberUnit;

    public String getNameUnit() {
        return nameUnit;
    }

    public void setNameUnit(String nameUnit) {
        this.nameUnit = nameUnit;
    }

    public String  getNumberUnit() {
        return numberUnit;
    }

    public void setNumberUnit(String  numberUnit) {
        this.numberUnit = numberUnit;
    }
}
