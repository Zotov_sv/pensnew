package ru.pens.spis_ozac.createOzac;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by I on 16.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OzacOutputInventoryTerritorialAuthorityPFR {

    @XmlElement (name = "НалоговыйНомер")
    private OzacOutputInventoryTerritorialAuthorityPFRTaxNumber pfrTaxNumber;

    @XmlElement (name = "НаименованиеОрганизации")
    private String nameOfCompany;

    @XmlElement (name = "РегистрационныйНомер")
    private String registrationNumber;

    public OzacOutputInventoryTerritorialAuthorityPFRTaxNumber getPfrTaxNumber() {
        return pfrTaxNumber;
    }

    public void setPfrTaxNumber(OzacOutputInventoryTerritorialAuthorityPFRTaxNumber pfrTaxNumber) {
        this.pfrTaxNumber = pfrTaxNumber;
    }

    public String getNameOfCompany() {
        return nameOfCompany;
    }

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
