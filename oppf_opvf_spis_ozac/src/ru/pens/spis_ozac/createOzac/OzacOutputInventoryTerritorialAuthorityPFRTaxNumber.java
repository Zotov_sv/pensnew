package ru.pens.spis_ozac.createOzac;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by I on 16.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OzacOutputInventoryTerritorialAuthorityPFRTaxNumber {

    @XmlElement (name = "ИНН")
    private String inn;

    @XmlElement (name = "КПП")
    private String kpp;

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }
}
