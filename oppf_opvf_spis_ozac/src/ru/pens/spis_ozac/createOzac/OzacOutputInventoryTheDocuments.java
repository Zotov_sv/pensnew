package ru.pens.spis_ozac.createOzac;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by I on 16.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OzacOutputInventoryTheDocuments {

    @XmlElement (name = "Количество")
    private int quantity;

    @XmlElement (name = "НаличиеДокументов")
    private OzacOutputInventoryTheDocumentsAvailabilityOfDocuments availabilityOfDocuments;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public OzacOutputInventoryTheDocumentsAvailabilityOfDocuments getAvailabilityOfDocuments() {
        return availabilityOfDocuments;
    }

    public void setAvailabilityOfDocuments(OzacOutputInventoryTheDocumentsAvailabilityOfDocuments availabilityOfDocuments) {
        this.availabilityOfDocuments = availabilityOfDocuments;
    }
}
