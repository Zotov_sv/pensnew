package ru.pens.spis_ozac.createOzac;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by I on 16.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OzacOutputInventoryTheDocumentsAvailabilityOfDocuments {

    @XmlElement (name = "ТипДокумента")
    private String signDocument;

    @XmlElement (name = "Количество")
    private int quantity;

    public String getSignDocument() {
        return signDocument;
    }

    public void setSignDocument(String signDocument) {
        this.signDocument = signDocument;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
