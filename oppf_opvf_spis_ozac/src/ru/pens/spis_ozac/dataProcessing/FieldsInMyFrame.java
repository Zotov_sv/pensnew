package ru.pens.spis_ozac.dataProcessing;

import ru.pens.spis_ozac.parsSpis.SpisListPayDetailsRecipient;
import ru.pens.spis_ozac.parsSpis.Spis;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Created by Admin on 09.12.2016.
 */
public class FieldsInMyFrame {

    private static LinkedHashMap<String, Person> map = new LinkedHashMap<>();
    private static Map<String, String> listCodeDelivered = new LinkedHashMap();

    private FieldsInMyFrame() {}

    private static class MainXmlHelp {
        private static FieldsInMyFrame mainXML = new FieldsInMyFrame();
    }

    public static FieldsInMyFrame getMainXmlHelp() {
        return MainXmlHelp.mainXML;
    }

    public void fieldsInFrame(Spis spis) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = new Date();
        String today = simpleDateFormat.format(date);

        for (SpisListPayDetailsRecipient x : spis.getSpisInputDoc().getSpisListPay().getSpisListPayDetailsRecipientList()) {
            map.put(x.getIdArray(), new Person(Integer.valueOf(x.getIdArray()), x.getNumberPaymasterDeal(),
                    x.getAreaCode(), x.getInsuranceNumber(),
                    x.getSpisListPayDetailsRecipientFio(), x.getBill(), x.getSpisListPayDetailsRecipientAllPay(),
                    x.getDeliveredSum(), "31",today,today,"",""));
        }


        listCodeDelivered.put("31", "Зачисление на счет, указанный в списке выплат");
        listCodeDelivered.put("32", "Зачисление на вновь открытый счет взамен указанного в списке выплат ранее закрытого счета");
        listCodeDelivered.put("Н31", "Не зачисление на счет – отсутствие не соответствие номера счёта получателя");
        listCodeDelivered.put("Н32", "Не зачисление на счет – имеются расхождения в ФИО получателя ");
        listCodeDelivered.put("Н33", "Не зачисление на счет – счёт закрыт");
        listCodeDelivered.put("Н35", "Не зачисление на счет – нарушение условий по вкладу");
        listCodeDelivered.put("Н37", "Не зачисление на счет – отметка о смерти");

    }

    public LinkedHashMap<String, Person> getMap() {
        return map;
    }

    public Map<String, String> getListCodeDelivered() {
        return listCodeDelivered;
    }

}




