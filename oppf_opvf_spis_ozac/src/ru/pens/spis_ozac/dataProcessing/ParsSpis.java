package ru.pens.spis_ozac.dataProcessing;

import ru.pens.spis_ozac.parsSpis.Spis;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by Admin on 19.01.2017.
 */
public class ParsSpis {
   private Spis spis;

  private ParsSpis(){}

  private static class InputFieldsHelp{
      private static ParsSpis parsSpis = new ParsSpis();
  }
  public static ParsSpis getInputFields(){
      return InputFieldsHelp.parsSpis;
  }


  public void pars(String pathFile){
      try {
          JAXBContext jc = JAXBContext.newInstance(Spis.class);
          Unmarshaller unmarshaller = jc.createUnmarshaller();
          spis = (Spis) unmarshaller.unmarshal(new File(pathFile));
      } catch (Exception e) {
          System.out.print(e);
      }
  }

    public Spis getSpis() {
        return spis;
    }
}
