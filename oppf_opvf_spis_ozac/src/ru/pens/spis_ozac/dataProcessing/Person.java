package ru.pens.spis_ozac.dataProcessing;

import ru.pens.spis_ozac.parsSpis.SpisListPayDetailsRecipientFio;
import ru.pens.spis_ozac.parsSpis.SpisListPayDetailsRecipientAllPay;

import java.util.List;

public class Person {

    public int idArray;
    public String numberPaymasterDeal;
    public String areaCode;
    public String insuranceNumber;
    public List<SpisListPayDetailsRecipientFio> spisListPayDetailsRecipientFio;
    public String bill;
    public String billBank;
    public List<SpisListPayDetailsRecipientAllPay> pays;
    public double deliveredSum;
    public String enrollmentCode;
    public String actualDeliveryDate;
    public String today;
    public String note;


    Person(int idArray, String numberPaymasterDeal, String areaCode, String insuranceNumber, List<SpisListPayDetailsRecipientFio> spisListPayDetailsRecipientFio, String bill,
           List<SpisListPayDetailsRecipientAllPay> pays, double deliveredSum, String enrollmentCode, String actualDeliveryDate,String today,String billBank,String note) {

        this.idArray = idArray;
        this.numberPaymasterDeal = numberPaymasterDeal;
        this.areaCode = areaCode;
        this.insuranceNumber = insuranceNumber;
        this.spisListPayDetailsRecipientFio = spisListPayDetailsRecipientFio;
        this.bill = bill;
        this.pays = pays;
        this.deliveredSum = deliveredSum;
        this.enrollmentCode = enrollmentCode;
        this.actualDeliveryDate = actualDeliveryDate;
        this.today = today;
        this.billBank = billBank;
        this.note = note;

    }
}
