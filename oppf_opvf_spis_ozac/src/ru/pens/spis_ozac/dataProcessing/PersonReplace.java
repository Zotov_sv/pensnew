package ru.pens.spis_ozac.dataProcessing;


import ru.pens.spis_ozac.dataProcessing.Person;

import java.util.Map;

/**
 * Created by Admin on 14.12.2016.
 */

public class PersonReplace {
    public static void replace(Map<String, Person> map, String idGetText, String enrollmentCodeGetText, String billGetText,String actualDeliveryDate,String billBank,String note) {
        map.get(idGetText).enrollmentCode = enrollmentCodeGetText;
        map.get(idGetText).bill = billGetText;
        map.get(idGetText).actualDeliveryDate=actualDeliveryDate;
        if (!billBank.equals("")){
            map.get(idGetText).billBank = billBank;
        }
        if (!note.equals("")){
            map.get(idGetText).note = note;
        }
    }
}
