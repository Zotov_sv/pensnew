package ru.pens.spis_ozac.frames;

import ru.pens.oppf_opvf.parsOppf.OppfListTransferredFilesEnrollmentTrFiles;
import ru.pens.oppf_opvf.dataProcessing.ParsOppf;
import ru.pens.spis_ozac.dataProcessing.FieldsInMyFrame;
import ru.pens.spis_ozac.dataProcessing.ParsSpis;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by Admin on 27.02.2017.
 */
public class AlertFiles extends JFrame{
    private JPanel jPanel;
    private JPanel jTextPanel;
    private JPanel jButtonPanel;
    private JTextPane jText;
    private JButton jCancelButton;
    private JButton jFormationButton;
    private int counter = 0;
    public AlertFiles(String chooseFiles,String outputFiles,ArrayList<String> listNameAllFiles) {
        jCancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        jFormationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {

                    ParsOppf.pars(chooseFiles);
                    Path path = Paths.get(chooseFiles);
                    for (OppfListTransferredFilesEnrollmentTrFiles x : ParsOppf.getOppf().getOppfInputDoc().getTransferredFiles().getTrFiles()){
                        counter++;

                    }

                    ParsSpis.getInputFields().pars(path.getParent() + "\\" + ParsOppf.getOppf().getOppfInputDoc().getTransferredFiles().getTrFiles().iterator().next().getNameFile());
                    FieldsInMyFrame.getMainXmlHelp().fieldsInFrame(ParsSpis.getInputFields().getSpis());

                    //new FrameCustomerData(FieldsInMyFrame.getMainXmlHelp().getMap(), FieldsInMyFrame.getMainXmlHelp().getListCodeDelivered(), outputFiles,listNameAllFiles);


                    System.out.print(counter);

                }
                catch (Exception e1) {
                    JOptionPane.showMessageDialog(null,"Выбери файл с окончанием OPPF","Message header", JOptionPane.DEFAULT_OPTION );
                }

            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        add(jPanel);
        setVisible(true);
        pack();
        setLocationRelativeTo(null);
    }
}
