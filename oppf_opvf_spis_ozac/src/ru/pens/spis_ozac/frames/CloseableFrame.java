package ru.pens.spis_ozac.frames;

/**
 * Created by Admin on 02.03.2017.
 */
public interface CloseableFrame {
    void close(int position);
}
