package ru.pens.spis_ozac.frames;


import ru.pens.global.counter.AllCounter;
import ru.pens.global.dataProcessing.OutputFields;
import ru.pens.spis_ozac.dataProcessing.Person;
import ru.pens.spis_ozac.dataProcessing.PersonReplace;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Host on 29.01.2017.
 */
public class FrameCustomerData extends JFrame {
    private JPanel jPanelMain;
    private JPanel jPanelComboBox;
    private JComboBox jComboBox;
    private JPanel jPanelLabel;
    private JPanel jPanelText;
    private JTextField jTextId;
    private JTextField jTextPayDeal;
    private JTextField jTextAreaCode;
    private JTextField jTextInsuranceNumber;
    private JTextField jTextLastName;
    private JTextField jTextFirstName;
    private JTextField jTextMiddleName;
    private JTextField jTextBill;
    private JTextField jTextAmountDelivered;
    private JTextField jTextActualDeliveryDate;
    private JComboBox comboBox1;
    private JTextField jTextEnrollmentCode;
    private JButton jButtonRecordDetails;
    private JButton jButtonNext;
    private JTextField jTextToday;
    private JPanel jPanelButton;
    private JPanel jPanelIfExist32;
    private JTextField jTextFieldBankBill;
    private JTextPane jTextPaneNote;
    private CloseableFrame closeableFrame;
    private int countPosition;
    private static int allCounter;
    public FrameCustomerData(LinkedHashMap<String, Person> map, Map<String, String> mapDelivered, String outPath,ArrayList<String> listNameInputFiles) {

        jPanelIfExist32.setVisible(false);
        for (Map.Entry x : map.entrySet()) {
            Person person = (Person) x.getValue();
            jComboBox.addItem(person.idArray + " " + person.spisListPayDetailsRecipientFio.iterator().next().getLastName() +
                    " " + person.spisListPayDetailsRecipientFio.iterator().next().getFirstName() + " " + person.spisListPayDetailsRecipientFio.iterator().next().getMiddleName());
        }
        for (Map.Entry x : mapDelivered.entrySet()) {
            String ad = (String) x.getKey() + " " + x.getValue();
            comboBox1.addItem(ad);
            comboBox1.setEditable(false);
        }

        jButtonNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                allCounter= AllCounter.allCounter().receivingCount();

                if (jTextBill.getText().length() == 0 | jTextActualDeliveryDate.getText().length() == 0 | jTextEnrollmentCode.getText().length() == 0) {
                    JOptionPane.showMessageDialog(null, "Пожалуйста, проверьте заполнение данных полей: Дата зачисления, Номер счёта в банке, Код доставки", "Message header", JOptionPane.DEFAULT_OPTION);
                } else {
                    try {
                        setVisible(false);
                        new OutputFields();
                        for (Map.Entry<String, Person> x : map.entrySet()) {
                            Person person = (Person) x.getValue();
                            if (!person.enrollmentCode.equals("31") & !person.enrollmentCode.equals("32")) {
                                FrameOutputInventoryIfNot31 fm1 = new FrameOutputInventoryIfNot31(outPath,listNameInputFiles,allCounter);
                                fm1.setCloseableFrame(closeableFrame);
                                fm1.setCountPosition(countPosition);
                                return;
                            }

                        }
                        FrameOutputInventory rt = new FrameOutputInventory(outPath,listNameInputFiles,allCounter);
                        rt.setCloseableFrame(closeableFrame);
                        rt.setCountPosition(countPosition);

                    } catch (Exception e1) {
                        System.out.print(e1);
                    }
                }
            }
        });

        jButtonRecordDetails.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PersonReplace.replace(map, jTextId.getText(), jTextEnrollmentCode.getText(), jTextBill.getText(), jTextActualDeliveryDate.getText(),jTextFieldBankBill.getText(),jTextPaneNote.getText());
            }
        });

        jComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox box = (JComboBox) e.getSource();
                String item = (String) box.getSelectedItem();
                for (Map.Entry x : map.entrySet()) {
                    Person person = (Person) x.getValue();
                    String text = person.idArray + " " + person.spisListPayDetailsRecipientFio.iterator().next().getLastName() +
                            " " + person.spisListPayDetailsRecipientFio.iterator().next().getFirstName() + " " + person.spisListPayDetailsRecipientFio.iterator().next().getMiddleName();
                    if (item.equals(text)) {
                        jTextId.setText(String.valueOf(person.idArray));
                        jTextPayDeal.setText(person.numberPaymasterDeal);
                        jTextAreaCode.setText(person.areaCode);
                        jTextInsuranceNumber.setText(person.insuranceNumber);
                        jTextLastName.setText(person.spisListPayDetailsRecipientFio.iterator().next().getLastName());
                        jTextFirstName.setText(person.spisListPayDetailsRecipientFio.iterator().next().getFirstName());
                        jTextMiddleName.setText(person.spisListPayDetailsRecipientFio.iterator().next().getMiddleName());
                        jTextBill.setText(person.bill);
                        jTextAmountDelivered.setText(String.valueOf(person.deliveredSum));
                        jTextEnrollmentCode.setText(person.enrollmentCode);
                        jTextActualDeliveryDate.setText(person.actualDeliveryDate);
                        jTextToday.setText(person.today);
                        jTextFieldBankBill.setText(person.billBank);
                        jTextPaneNote.setText(person.note);


                    }
                }
            }
        });

        comboBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox box = (JComboBox) e.getSource();
                String item = (String) box.getSelectedItem();
                for (Map.Entry x : mapDelivered.entrySet()) {
                    String text = (String) x.getKey() + " " + x.getValue();
                    if (item.equals(text)) {
                        jTextEnrollmentCode.setText(String.valueOf(x.getKey()));
                        if (x.getKey().equals("32")){
                            jPanelIfExist32.setVisible(true);
                        }
                        else {
                            jPanelIfExist32.setVisible(false);
                        }
                    }

                }
            }
        });


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setBounds(0, 0, 618, 510);
        setResizable(false);
        add(jPanelMain);
        //pack();
        setLocationRelativeTo(null);

    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public void setCloseableFrame(CloseableFrame closeableFrame) {
        this.closeableFrame = closeableFrame;
    }

    public void setCountPosition(int countPosition) {
        this.countPosition = countPosition;
    }
}
