package ru.pens.spis_ozac.frames;

import ru.pens.global.dataProcessing.OutputFields;
import ru.pens.spis_ozac.withFiles.CreateXML;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by Admin on 30.01.2017.
 */
public class FrameOutputInventory extends JFrame {
    OutputFields outputFields = new OutputFields();

    private JPanel jPanelMain;
    private JPanel jPanelLabelAndText;
    private JTextField jTextNameCompany;
    private JTextField jTextSignDocuments;
    private JTextField jTextNumberBank;
    private JTextField jTextCompanyFormationDocument;
    private JTextField jTextSignArraysOrder;
    private JTextField jTextMonth;
    private JTextField jTextYear;
    private JTextField jTextAmountEnrolledFilial;
    private JTextField jTextAmountRecipientEnrolled;
    private JTextField jTextAmountNotEnrolledFilial;
    private JTextField jTextAmountRecipientNotEnrolled;
    private JTextField jTextOutputNumber;
    protected JTextField jTextDateFormation;
    private JPanel jPaneHeader;
    private JLabel jLabelHeader;
    private JPanel jPanelAuthority;
    private JTextField jTextAuthorityPfrNameCompany;
    private JTextField jTextAuthorityPfrInn;
    private JTextField jTextAuthorityPfrKpp;
    private JTextField jTextAuthorityPfrRegistrationNumber;
    private JPanel jPanelHeaderPfr;
    private JLabel jLabelHeaderPfr;
    private JTextField jTextPosition;
    private JTextField jTextHead;
    private JPanel jPanelPaymentOrder;
    protected JTextField jTextNumberPaymentOrder;
    protected JTextField jTextDatePaymentOrder;
    protected JTextField jTextAmountPaymentOrder;
    protected JButton jButtonNext;
    private JTextArea jTextArea;
    private CloseableFrame closeableFrame;
    private int countPosition;
    protected HashMap<String, String> map = new LinkedHashMap<>();


    public FrameOutputInventory(String outPath,ArrayList<String> listNameInputFiles,int allCounter) {

        jTextArea.setEditable(false);

        jTextNameCompany.setText(outputFields.getNameOfCompany());
        jTextSignDocuments.setText(outputFields.getSignDocuments());
        jTextNumberBank.setText(outputFields.getNumberBankAndMessage());
        jTextCompanyFormationDocument.setText(outputFields.getNameOfCompany());
        jTextSignArraysOrder.setText(outputFields.getSignArraysOrder());
        jTextMonth.setText(outputFields.getMonth());
        jTextYear.setText(outputFields.getYear());
        jTextAmountEnrolledFilial.setText(String.valueOf(outputFields.getAmountEnrolledFilial()));
        jTextAmountRecipientEnrolled.setText(String.valueOf(outputFields.getAmountRecipientEnrolled()));
        jTextAmountNotEnrolledFilial.setText(String.valueOf(outputFields.getAmountNotEnrolledFilial()));
        jTextAmountRecipientNotEnrolled.setText(String.valueOf(outputFields.getAmountRecipientNotEnrolled()));
        jTextOutputNumber.setText(String.valueOf(allCounter));
        jTextDateFormation.setText(outputFields.getDateFormation());
        jTextPosition.setText(outputFields.getPosition());
        jTextHead.setText(outputFields.getHead());

        jTextAuthorityPfrNameCompany.setText(outputFields.getAuthorityPfrNameCompany());
        jTextAuthorityPfrInn.setText(outputFields.getAuthorityPfrInn());
        jTextAuthorityPfrKpp.setText(outputFields.getAuthorityPfrKpp());
        jTextAuthorityPfrRegistrationNumber.setText(outputFields.getAuthorityPfrRegistrationNumber());

        map.put("NameCompany", jTextNameCompany.getText());
        map.put("INN", "7707083011");
        map.put("KPP", "662343001");
        map.put("Registration Number", "7707083011");
        map.put("Unit Company", "Нижнетагильский филиал АКБ Мосуралбанк (АО)");
        map.put("Unit RegistrationNumber", "7707083011");
        map.put("SignDocuments", jTextSignDocuments.getText());
        map.put("NumberBank", jTextNumberBank.getText());
        map.put("CompanyFormationDocument", jTextCompanyFormationDocument.getText());
        map.put("SignArraysOrder", jTextSignArraysOrder.getText());
        map.put("Month", jTextMonth.getText());
        map.put("Year", jTextYear.getText());
        map.put("AmountEnrolledFilial", jTextAmountEnrolledFilial.getText());
        map.put("AmountRecipientEnrolled", jTextAmountRecipientEnrolled.getText());
        map.put("AmountNotEnrolledFilial", jTextAmountNotEnrolledFilial.getText());
        map.put("AmountRecipientNotEnrolled", jTextAmountRecipientNotEnrolled.getText());
        map.put("OutputNumber", jTextOutputNumber.getText());
        map.put("DateFormation", jTextDateFormation.getText());
        map.put("TimeFormation", outputFields.getTimeFormation());
        map.put("Position", jTextPosition.getText());
        map.put("Head", jTextHead.getText());
        map.put("SystemNumberArray",outputFields.getSystemNumberArray());

        map.put("AuthorityPfrNameCompany", jTextAuthorityPfrNameCompany.getText());
        map.put("AuthorityPfrInn", jTextAuthorityPfrInn.getText());
        map.put("AuthorityPfrKpp", jTextAuthorityPfrKpp.getText());
        map.put("AuthorityPfrRegistrationNumber", jTextAuthorityPfrRegistrationNumber.getText());


        jButtonNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

               map.put("DateFormation", jTextDateFormation.getText());



                if (jTextOutputNumber.getText().length() == 0 | jTextAmountEnrolledFilial.getText().length() == 0 | jTextAmountNotEnrolledFilial.getText().length() == 0) {
                    JOptionPane.showMessageDialog(null, "Пожалуйста, проверьте заполнение полей: Исходящий номер, Сумма зачислено по Филиалу, Сумма не зачислено по Филиалу", "Message header", JOptionPane.DEFAULT_OPTION);
                } else {
                    if (!map.get("OutputNumber").equals(jTextOutputNumber.getText())) {
                        map.put("OutputNumber", jTextOutputNumber.getText());
                    }
                    new CreateXML(outPath, map,listNameInputFiles,allCounter);

                    setVisible(false);
                    closeableFrame.close(countPosition);
                }

            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setResizable(false);
        add(jPanelMain);
        pack();
        setLocationRelativeTo(null);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public void setCloseableFrame(CloseableFrame closeableFrame) {
        this.closeableFrame = closeableFrame;
    }

    public void setCountPosition(int countPosition) {
        this.countPosition = countPosition;
    }

    public HashMap<String, String> getMap() {
        return map;
    }
}
