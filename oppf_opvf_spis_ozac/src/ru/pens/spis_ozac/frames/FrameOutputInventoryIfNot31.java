package ru.pens.spis_ozac.frames;

import ru.pens.spis_ozac.withFiles.CreateXML;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by Admin on 31.01.2017.
 */
public class FrameOutputInventoryIfNot31 extends FrameOutputInventory {
    private CloseableFrame closeableFrame;
    private int countPosition;


    @Override
    public void setCloseableFrame(CloseableFrame closeableFrame) {
        this.closeableFrame = closeableFrame;
    }

    @Override
    public void setCountPosition(int countPosition) {
        this.countPosition = countPosition;
    }

    public FrameOutputInventoryIfNot31(String outPath, ArrayList<String> listNameAllFiles,int allCounter) {
        super(outPath,listNameAllFiles,allCounter);

        for (ActionListener a : jButtonNext.getActionListeners()){
            jButtonNext.removeActionListener(a);
        }

        jButtonNext.addActionListener(new ActionListener() {


            @Override
            public void actionPerformed(ActionEvent e) {
                map.put("DateFormation", jTextDateFormation.getText());
               new CreateXML(outPath, map, jTextNumberPaymentOrder.getText(),
                        jTextDatePaymentOrder.getText(),
                        jTextAmountPaymentOrder.getText(),listNameAllFiles,allCounter);
                setVisible(false);
                closeableFrame.close(countPosition);
            }
        });

    }
}

