package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.*;

/**
 * Created by Admin on 08.12.2016.
 */
@XmlRootElement(name = "ФайлПФР")
@XmlAccessorType(XmlAccessType.FIELD)
public class Spis {

    @XmlElement(name = "ИмяФайла")
    private String nameFile;

    @XmlElement(name = "ЗаголовокФайла")
    private String headerFile;

    @XmlElement(name = "ПачкаВходящихДокументов")
    private SpisInputDoc spisInputDoc;

    public SpisInputDoc getSpisInputDoc() {
        return spisInputDoc;
    }

}
