package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by I on 17.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisHeaderFile {

    @XmlElement(name = "ВерсияФормата")
    private String versionFormat;

    @XmlElement(name = "ТипФайла")
    private String signFile;

    @XmlElement(name = "ПрограммаПодготовкиДанных")
    private SpisHeaderFileTrainingProgramData programData;

    @XmlElement(name = "ИсточникДанных")
    private String dataSource;


    public void setVersionFormat(String versionFormat) {
        this.versionFormat = versionFormat;
    }

    public void setSignFile(String signFile) {
        this.signFile = signFile;
    }

    public void setProgramData(SpisHeaderFileTrainingProgramData programData) {
        this.programData = programData;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }
}
