package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by I on 17.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisHeaderFileTrainingProgramData {

    @XmlElement(name = "НазваниеПрограммы")
    private String nameOfMyProgram;

    @XmlElement(name = "Версия")
    private String version;


    public void setNameOfMyProgram(String nameOfMyProgram) {
        this.nameOfMyProgram = nameOfMyProgram;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
