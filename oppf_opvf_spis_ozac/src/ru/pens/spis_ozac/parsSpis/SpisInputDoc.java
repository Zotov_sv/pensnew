package ru.pens.spis_ozac.parsSpis;


import javax.xml.bind.annotation.*;


/**
 * Created by Admin on 09.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisInputDoc {

    @XmlElement(name = "ВХОДЯЩАЯ_ОПИСЬ")
    private SpisInputInventory spisInputInventory;

    @XmlElement(name = "СПИСОК_НА_ЗАЧИСЛЕНИЕ")
    private SpisListPay spisListPay;

    public SpisInputInventory getSpisInputInventory() {
        return spisInputInventory;
    }

    public SpisListPay getSpisListPay() {
        return spisListPay;
    }
}
