package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.*;
import java.util.List;


/**
 * Created by Admin on 09.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisInputInventory {

    @XmlElement(name = "НомерВпачке")
    private int numberStack;

    @XmlElement(name = "ТипВходящейОписи")
    private String signInventory;

    @XmlElement(name = "СоставительПачки")
    private List<SpisInputInventoryCompiliedPacks> spisInputInventoryCompiliedPacksList;

    @XmlElement(name = "НомерПачки")
    private SpisInputInventoryTwinPack spisInputInventoryTwinPack;

    @XmlElement(name = "СоставДокументов")
    private List<SpisInputInventoryDocumentComposition> spisInputInventoryDocumentCompositionList;

    @XmlElement(name = "ДатаСоставления")
    private String DateOfPreparation;

    @XmlElement(name = "ТерриториальныйОрганПФР")
    private List<SpisInputInventoryTerritorialAuthority> spisInputInventoryTerritorialAuthorityList;

    @XmlElement(name = "НомерБанка")
    private String numberBank;

    @XmlElement(name = "Банк")
    private SpisInputInventoryBank spisInputInventoryBank;

    @XmlElement(name = "НомерПлатежногоПоручения")
    private String numberPaymentOrder;

    @XmlElement(name = "ДатаПлатежногоПоручения")
    private String datePaymentOrder;

    @XmlElement(name = "НомерДоговораОзачисленииСуммПенсий")
    private String contractNumber;

    @XmlElement(name = "ДатаДоговораОзачисленииСуммПенсий")
    private String dateContractNumber;

    @XmlElement(name = "БанковскиеРеквизитыДляВозвратаСумм")
    private SpisInputInventoryBankDetailsForTheRefundAmount refundAmount;

    @XmlElement(name = "СистемныйНомерМассива")
    private String systemNumberArray;

    @XmlElement(name = "ТипМассиваПоручений")
    private String signArrayOrder;

    @XmlElement(name = "Месяц")
    private String month;

    @XmlElement(name = "Год")
    private String year;

    @XmlElement(name = "ОбщаяСуммаПоМассиву")
    private double totalAmount;

    @XmlElement(name = "ОбщееКоличествоПорученийПоМассиву")
    private int totalOrderArrays;

    @XmlElement(name = "КоличествоЧастейМассива")
    private String quantityArrays;

    @XmlElement(name = "НомерЧастиМассива")
    private String numberStackArrays;

    @XmlElement(name = "СуммаПоЧастиМассива")
    private String amountStackArrays;

    @XmlElement(name = "КоличествоПорученийПоЧастиМассива")
    private String getQuantityOrderStackArrays;

    @XmlElement(name = "Должность")
    private String position;

    @XmlElement(name = "Руководитель")
    private String head;

    public int getNumberStack() {
        return numberStack;
    }

    public SpisInputInventoryTwinPack getSpisInputInventoryTwinPack() {
        return spisInputInventoryTwinPack;
    }

    public List<SpisInputInventoryTerritorialAuthority> getSpisInputInventoryTerritorialAuthorityList() {
        return spisInputInventoryTerritorialAuthorityList;
    }

    public String getNumberBank() {
        return numberBank;
    }

    public String getMonth() {
        return month;
    }

    public String getYear() {
        return year;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public int getTotalOrderArrays() {
        return totalOrderArrays;
    }

    public String getSignInventory() {
        return signInventory;
    }

    public List<SpisInputInventoryCompiliedPacks> getSpisInputInventoryCompiliedPacksList() {
        return spisInputInventoryCompiliedPacksList;
    }

    public List<SpisInputInventoryDocumentComposition> getSpisInputInventoryDocumentCompositionList() {
        return spisInputInventoryDocumentCompositionList;
    }

    public String getDateOfPreparation() {
        return DateOfPreparation;
    }

    public SpisInputInventoryBank getSpisInputInventoryBank() {
        return spisInputInventoryBank;
    }

    public String getNumberPaymentOrder() {
        return numberPaymentOrder;
    }

    public String getDatePaymentOrder() {
        return datePaymentOrder;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public String getDateContractNumber() {
        return dateContractNumber;
    }

    public SpisInputInventoryBankDetailsForTheRefundAmount getRefundAmount() {
        return refundAmount;
    }

    public String getSystemNumberArray() {
        return systemNumberArray;
    }

    public String getSignArrayOrder() {
        return signArrayOrder;
    }

    public String getQuantityArrays() {
        return quantityArrays;
    }

    public String getNumberStackArrays() {
        return numberStackArrays;
    }

    public String getAmountStackArrays() {
        return amountStackArrays;
    }

    public String getGetQuantityOrderStackArrays() {
        return getQuantityOrderStackArrays;
    }

    public String getPosition() {
        return position;
    }

    public String getHead() {
        return head;
    }
}
