package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 15.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisInputInventoryBank {

    @XmlElement(name = "НаименованиеОрганизации")
    private String nameCompany;

    @XmlElement(name = "РегистрационныйНомер")
    private String registrationNumber;

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }
}
