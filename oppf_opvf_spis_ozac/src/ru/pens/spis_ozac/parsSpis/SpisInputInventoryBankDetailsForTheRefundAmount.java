package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by I on 19.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisInputInventoryBankDetailsForTheRefundAmount {

    @XmlElement(name = "БанкПФР")
    private SpisInputInventoryBankDetailsForTheRefundAmountBankPfr refundAmountBankPfr;

    @XmlElement(name = "РасчетныйСчет")
    private String bill;

    @XmlElement(name = "ОКАТО")
    private String okato;

    @XmlElement(name = "КорреспондентскийСчет")
    private String correspondentAcount;

    @XmlElement(name = "БИК")
    private String bik;

}
