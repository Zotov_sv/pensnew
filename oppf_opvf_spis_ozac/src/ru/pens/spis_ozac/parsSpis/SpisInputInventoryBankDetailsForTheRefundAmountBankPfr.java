package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by I on 19.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisInputInventoryBankDetailsForTheRefundAmountBankPfr {

    @XmlElement(name = "НалоговыйНомер")
    private SpisInputInventoryBankDetailsForTheRefundAmountBankPfrTaxNumber amountBankPfrTaxNumber;

    @XmlElement(name = "НаименованиеОрганизации")
    private String nameOfCompany;

    @XmlElement(name = "РегистрационныйНомер")
    private String registrationNumber;

    public String getNameOfCompany() {
        return nameOfCompany;
    }

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
