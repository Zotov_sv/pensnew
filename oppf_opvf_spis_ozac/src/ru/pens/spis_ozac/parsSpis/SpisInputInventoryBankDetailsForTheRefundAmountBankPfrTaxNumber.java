package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by I on 19.12.2016.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class SpisInputInventoryBankDetailsForTheRefundAmountBankPfrTaxNumber {

    @XmlElement(name = "ИНН")
    private String inn;

    @XmlElement(name = "КПП")
    private String kpp;

}
