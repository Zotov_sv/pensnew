package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 15.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisInputInventoryCompiliedPacksTaxNumber {

    @XmlElement(name = "ИНН")
    private String INN;

    @XmlElement(name = "КПП")
    private String KPP;

    public String getINN() {
        return INN;
    }

    public String getKPP() {
        return KPP;
    }

}
