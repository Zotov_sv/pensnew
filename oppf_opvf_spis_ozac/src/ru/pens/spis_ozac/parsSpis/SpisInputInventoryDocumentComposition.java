package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by Admin on 15.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisInputInventoryDocumentComposition {

    @XmlElement(name = "Количество")
    private String quantity;

    @XmlElement(name = "НаличиеДокументов")
    private List<SpisInputInventoryDocumentCompositionAvailabilityOfDocuments> availabilityOfDocumentsList;

}
