package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 15.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisInputInventoryDocumentCompositionAvailabilityOfDocuments {

    @XmlElement(name = "ТипДокумента")
    private String signDocument;

    @XmlElement(name = "Количество")
    private String quantity;

}
