package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 15.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisInputInventoryTerritorialAuthority {

    @XmlElement(name = "НалоговыйНомер")
    private SpisInputInventoryCompiliedPacksTaxNumber spisInputInventoryCompiliedPacksTaxNumber;

    @XmlElement(name = "НаименованиеОрганизации")
    private String nameOfCompany;

    @XmlElement(name = "РегистрационныйНомер")
    private String registrationNumber;

    public SpisInputInventoryCompiliedPacksTaxNumber getSpisInputInventoryCompiliedPacksTaxNumber() {
        return spisInputInventoryCompiliedPacksTaxNumber;
    }

    public String getNameOfCompany() {
        return nameOfCompany;
    }

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}

