package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 15.12.2016.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class SpisInputInventoryTwinPack {

    @XmlElement(name = "Основной")
    private int main;

    public int getMain() {
        return main;
    }

}
