package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by Admin on 09.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisListPay {

    @XmlElement(name = "НомерВпачке")
    private String numberStack;

    @XmlElement(name = "СведенияОполучателе")
    private List<SpisListPayDetailsRecipient> spisListPayDetailsRecipientList;

    public List<SpisListPayDetailsRecipient> getSpisListPayDetailsRecipientList() {
        return spisListPayDetailsRecipientList;
    }

}
