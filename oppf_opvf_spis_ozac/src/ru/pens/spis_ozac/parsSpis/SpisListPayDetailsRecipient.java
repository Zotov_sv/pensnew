package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.*;
import java.util.List;


/**
 * Created by Admin on 09.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisListPayDetailsRecipient {

    @XmlElement(name = "НомерВмассиве")
    private String idArray;

    @XmlElement(name = "НомерВыплатногоДела")
    private String numberPaymasterDeal;

    @XmlElement(name = "КодРайона")
    private String areaCode;

    @XmlElement(name = "СтраховойНомер")
    private String insuranceNumber;

    @XmlElement(name = "ФИО")
    private List<SpisListPayDetailsRecipientFio> spisListPayDetailsRecipientFio;

    @XmlElement(name = "НомерСчета")
    private String bill;

    @XmlElement(name = "ВсеВыплаты")
    private List<SpisListPayDetailsRecipientAllPay> spisListPayDetailsRecipientAllPay;

    @XmlElement(name = "СуммаКдоставке")
    private double deliveredSum;

    public String getIdArray() {
        return idArray;
    }

    public String getNumberPaymasterDeal() {
        return numberPaymasterDeal;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public List<SpisListPayDetailsRecipientFio> getSpisListPayDetailsRecipientFio() {
        return spisListPayDetailsRecipientFio;
    }

    public String getBill() {
        return bill;
    }

    public List<SpisListPayDetailsRecipientAllPay> getSpisListPayDetailsRecipientAllPay() {
        return spisListPayDetailsRecipientAllPay;
    }

    public double getDeliveredSum() {
        return deliveredSum;
    }


}
