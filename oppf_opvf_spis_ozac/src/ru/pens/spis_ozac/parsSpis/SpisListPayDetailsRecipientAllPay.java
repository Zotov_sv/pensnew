package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by Admin on 09.12.2016.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class SpisListPayDetailsRecipientAllPay {

    @XmlElement(name = "Количество")
    private int quantity;

    @XmlElement(name = "Выплата")
    private List<SpisListPayDetailsRecipientAllPayPay> spisListPayDetailsRecipientAllPayPays;

    public int getQuantity() {
        return quantity;
    }

}
