package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.*;


/**
 * Created by Admin on 09.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisListPayDetailsRecipientAllPayPay {

    @XmlElement(name = "ПризнакВыплаты")
    private String paySign;

    @XmlElement(name = "СуммаКвыплате")
    private String paySumm;

    @XmlElement(name = "ДатаНачалаПериода")
    private String payStart;

    @XmlElement(name = "ДатаКонцаПериода")
    private String payEnd;

    @XmlElement(name = "ВидВыплатыПоПЗ")
    private String payView;

}
