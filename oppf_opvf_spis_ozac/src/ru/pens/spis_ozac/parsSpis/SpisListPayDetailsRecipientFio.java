package ru.pens.spis_ozac.parsSpis;

import javax.xml.bind.annotation.*;


/**
 * Created by Admin on 09.12.2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpisListPayDetailsRecipientFio {
    @XmlElement(name = "Фамилия")
    private String lastName;
    @XmlElement(name = "Имя")
    private String firstName;
    @XmlElement(name = "Отчество")
    private String middleName;

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }
}
