package ru.pens.spis_ozac.withFiles;


import ru.pens.oppf_opvf.withFiles.OpvfBaseXml;

import ru.pens.oppf_opvf.dataProcessing.ParsOppf;
import ru.pens.spis_ozac.createOzac.*;
import ru.pens.spis_ozac.dataProcessing.FieldsInMyFrame;
import ru.pens.spis_ozac.dataProcessing.ParsSpis;
import ru.pens.spis_ozac.dataProcessing.Person;

import ru.pens.spis_ozac.parsSpis.SpisHeaderFile;
import ru.pens.spis_ozac.parsSpis.SpisHeaderFileTrainingProgramData;

import ru.pens.spis_ozac.parsSpis.Spis;
import ru.pens.spra.withFiles.CreateSpra;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Admin on 13.12.2016.
 */
public class CreateXML {

    private String nameFile;
    private static ArrayList<String> listNameOutputFiles = new ArrayList<>();
    private ArrayList<String> listNameOuputFilesSpra = new ArrayList<>();

    //public static int tempNewOutputNumber;
    //static String tmpNewOutputNumber = "";
    private static int count = 0;
    private static int countSpra = 0;
    private static int tmpCountSpra = 0;

    public CreateXML(String outputPath, HashMap<String,
                     String> hashMap, String NumberPaymentOrder,
                     String DatePaymentOrder, String AmountPaymentOrder,
                     ArrayList<String> listNameInputFiles,int outputNumber) {

        LinkedHashMap<String, Person> map = FieldsInMyFrame.getMainXmlHelp().getMap();
        Spis originalSpis = ParsSpis.getInputFields().getSpis();
        ArrayList<OzacElementsDetails> linkedHashMap = new ArrayList<>();

        int allQuantity = 0;
        for (Map.Entry x : map.entrySet()) {
            Person person = (Person) x.getValue();
            OzacElementsDetails xmlDetails = new OzacElementsDetails();
            xmlDetails.setIdArray(person.idArray);
            xmlDetails.setNumberPaymasterDeal(person.numberPaymasterDeal);
            xmlDetails.setAreaCode(person.areaCode);
            xmlDetails.setInsuranceNumber(person.insuranceNumber);
            xmlDetails.setNameSpisListPayDetailsRecipientFio(person.spisListPayDetailsRecipientFio);
            xmlDetails.setBill(person.bill);
            xmlDetails.setPays(person.pays);
            xmlDetails.setDeliveredSum(person.deliveredSum);
            xmlDetails.setEnrollmentCode(person.enrollmentCode);
            if (person.enrollmentCode.equals("31") | person.enrollmentCode.equals("32")){
                xmlDetails.setActualDeliveryDate(person.actualDeliveryDate);
                if (person.enrollmentCode.equals("32")){
                    tmpCountSpra = 1;
                }
            }
            xmlDetails.setToday(person.today);
            allQuantity = allQuantity + person.pays.iterator().next().getQuantity();
            linkedHashMap.add(xmlDetails);
        }

        countSpra=countSpra+tmpCountSpra;

        OzacElements xmlElements = new OzacElements();
        xmlElements.setReportRecipient(linkedHashMap);

        OzacOutputInventory xmlOutputInventory = new OzacOutputInventory();
        OzacOutputInventoryCompiledPacks compiledPacks = new OzacOutputInventoryCompiledPacks();
        OzacOutputInventoryCompiledPacksTaxNumber packsTaxNumber = new OzacOutputInventoryCompiledPacksTaxNumber();
        packsTaxNumber.setInn(hashMap.get("INN"));
        packsTaxNumber.setKpp(hashMap.get("KPP"));
        compiledPacks.setPacksTaxNumber(packsTaxNumber);
        compiledPacks.setNameOfCompany(hashMap.get("NameCompany"));
        compiledPacks.setPacksRegistrationNumber(hashMap.get("Registration Number"));

        OzacOutputInventoryCompiledPacksUnit compiledPacksUnit = new OzacOutputInventoryCompiledPacksUnit();
        compiledPacksUnit.setNameUnit(hashMap.get("Unit Company"));
        compiledPacksUnit.setNumberUnit(hashMap.get("Unit RegistrationNumber"));
        compiledPacks.setCompiledPacksUnit(compiledPacksUnit);

        OzacOutputInventoryTheDocuments inventoryTheDocuments = new OzacOutputInventoryTheDocuments();
        inventoryTheDocuments.setQuantity(allQuantity);
        OzacOutputInventoryTheDocumentsAvailabilityOfDocuments availabilityOfDocuments = new OzacOutputInventoryTheDocumentsAvailabilityOfDocuments();
        availabilityOfDocuments.setSignDocument(hashMap.get("SignDocuments"));
        availabilityOfDocuments.setQuantity(allQuantity);
        inventoryTheDocuments.setAvailabilityOfDocuments(availabilityOfDocuments);
        OzacOutputInventoryTerritorialAuthorityPFR authorityPFR = new OzacOutputInventoryTerritorialAuthorityPFR();
        OzacOutputInventoryTerritorialAuthorityPFRTaxNumber authorityPFRTaxNumber = new OzacOutputInventoryTerritorialAuthorityPFRTaxNumber();
        authorityPFRTaxNumber.setInn(hashMap.get("AuthorityPfrInn"));
        authorityPFRTaxNumber.setKpp(hashMap.get("AuthorityPfrKpp"));
        authorityPFR.setPfrTaxNumber(authorityPFRTaxNumber);
        authorityPFR.setNameOfCompany(hashMap.get("AuthorityPfrNameCompany"));
        authorityPFR.setRegistrationNumber(hashMap.get("AuthorityPfrRegistrationNumber"));
        OzacOutputInventoryCompanyFormedDocuments formedDocuments = new OzacOutputInventoryCompanyFormedDocuments();
        formedDocuments.setNameOfCompany(hashMap.get("NameCompany"));
        xmlOutputInventory.setCompiliedPacks(compiledPacks);
        xmlOutputInventory.setOzacOutputInventoryTheDocuments(inventoryTheDocuments);
        xmlOutputInventory.setAuthorityPFR(authorityPFR);
        xmlOutputInventory.setNumberBank(hashMap.get("NumberBank"));
        xmlOutputInventory.setCompanyFormedDocuments(formedDocuments);

        try {
            if (!NumberPaymentOrder.equals(null) & !DatePaymentOrder.equals(null) & !AmountPaymentOrder.equals(null)) {
                xmlOutputInventory.setNumberPaymentOrder(NumberPaymentOrder);
                xmlOutputInventory.setDatePaymentOrder(DatePaymentOrder);
                xmlOutputInventory.setAmountPaymentOrder(AmountPaymentOrder);
            }
        } catch (Exception e) {

        }

        xmlOutputInventory.setSignArraysOrder(hashMap.get("SignArraysOrder"));
        xmlOutputInventory.setMonth(hashMap.get("Month"));
        xmlOutputInventory.setYear(hashMap.get("Year"));
        xmlOutputInventory.setAmountFilial(hashMap.get("AmountEnrolledFilial"));
        xmlOutputInventory.setRecipientEnrolled(hashMap.get("AmountRecipientEnrolled"));
        xmlOutputInventory.setNotAmountFilial(hashMap.get("AmountNotEnrolledFilial"));
        xmlOutputInventory.setRecipientNotEnrolled(hashMap.get("AmountRecipientNotEnrolled"));


        int tempNewOutputNumber = outputNumber + 1;
        String tmpNewOutputNumber = "";
        if (tempNewOutputNumber > 0 & tempNewOutputNumber <= 9) {
            tmpNewOutputNumber = "000000000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 9 & tempNewOutputNumber <= 99) {
            tmpNewOutputNumber = "00000000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 99 & tempNewOutputNumber <= 999) {
            tmpNewOutputNumber = "0000000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 999 & tempNewOutputNumber <= 9999) {
            tmpNewOutputNumber = "000000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 9999 & tempNewOutputNumber <= 99999) {
            tmpNewOutputNumber = "00000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 99999 & tempNewOutputNumber <= 999999) {
            tmpNewOutputNumber = "0000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 999999 & tempNewOutputNumber <= 9999999) {
            tmpNewOutputNumber = "000" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 9999999 & tempNewOutputNumber <= 99999999) {
            tmpNewOutputNumber = "00" + tempNewOutputNumber;
        } else if (tempNewOutputNumber > 99999999 & tempNewOutputNumber <= 999999999) {
            tmpNewOutputNumber = "0" + tempNewOutputNumber;
        }

        xmlOutputInventory.setOutputNumber(tmpNewOutputNumber);
        xmlOutputInventory.setDateFormation(hashMap.get("DateFormation"));
        xmlOutputInventory.setPosition(hashMap.get("Position"));
        xmlOutputInventory.setHead(hashMap.get("Head"));

        SpisHeaderFile spisHeaderFile = new SpisHeaderFile();
        spisHeaderFile.setVersionFormat("07.00");
        spisHeaderFile.setSignFile("ВНЕШНИЙ");
        SpisHeaderFileTrainingProgramData trainingProgramData = new SpisHeaderFileTrainingProgramData();
        trainingProgramData.setNameOfMyProgram("МосуралАйтиПродакшн");
        trainingProgramData.setVersion("ВЕРСИЯ1.12");
        spisHeaderFile.setProgramData(trainingProgramData);
        spisHeaderFile.setDataSource("ДОСТАВЩИК");


        OzacDocs xmlOutputDocument = new OzacDocs();

        xmlOutputDocument.setCreateXmlSpisInputInventory(originalSpis.getSpisInputDoc().getSpisInputInventory());
        xmlOutputDocument.setOzacOutputInventory(xmlOutputInventory);
        xmlOutputDocument.setOzacElements(xmlElements);


        Ozac filePFR = new Ozac();

        int numberStack = originalSpis.getSpisInputDoc().getSpisInputInventory().getNumberStack();
        int mainNumberStack = originalSpis.getSpisInputDoc().getSpisInputInventory().getSpisInputInventoryTwinPack().getMain();

        String tmpMainNumberStack;
        String tmpNumberStack;

        if (numberStack > 0 & numberStack <= 9) {
            tmpNumberStack = "00" + numberStack;
        } else if (numberStack > 9 & numberStack <= 99) {
            tmpNumberStack = "0" + numberStack;
        } else {
            tmpNumberStack = String.valueOf(numberStack);
        }

        if (mainNumberStack > 0 & mainNumberStack <= 9) {
            tmpMainNumberStack = "0000" + mainNumberStack;
        } else if (mainNumberStack > 9 & mainNumberStack <= 99) {
            tmpMainNumberStack = "000" + mainNumberStack;
        } else if (mainNumberStack > 99 & mainNumberStack <= 999) {
            tmpMainNumberStack = "00" + mainNumberStack;
        } else if (mainNumberStack > 999 & mainNumberStack <= 9999) {
            tmpMainNumberStack = "0" + mainNumberStack;
        } else {
            tmpMainNumberStack = String.valueOf(mainNumberStack);
        }




        nameFile = "OUT-700-Y-" + hashMap.get("Year") + "-ORG-" + hashMap.get("AuthorityPfrRegistrationNumber")
                + "-DIS-038-DCK-" + tmpMainNumberStack + "-" + tmpNumberStack + "-DOC-OZAC-FSB-"
                + hashMap.get("NumberBank") + "-OUTNMB-" + tmpNewOutputNumber + ".XML";
        filePFR.setNameFile(nameFile);
        filePFR.setSpisHeaderFile(spisHeaderFile);
        filePFR.setOzacDocs(xmlOutputDocument);



        try {

            File file = new File(outputPath + "\\" + nameFile);
            JAXBContext jc = JAXBContext.newInstance(Ozac.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(filePFR, file);
            //marshaller.marshal(filePFR, System.out);

            listNameOutputFiles.add(nameFile);

            if (tmpCountSpra == 1){
                CreateSpra.createSpra().createSpraXml(map,nameFile,hashMap,outputPath,listNameInputFiles,listNameOutputFiles,tmpCountSpra,count,tempNewOutputNumber);
                //new CreateSpra(map,nameFile,hashMap,outputPath,listNameInputFiles,listNameOutputFiles,listNameOuputFilesSpra);
                tmpCountSpra = 0;

            }
                if (ParsOppf.getOppf().getOppfInputDoc().getTransferredFiles().getTrFiles().size() == count+1 ) {
                    new OpvfBaseXml(nameFile, hashMap, outputPath,listNameInputFiles,listNameOutputFiles,CreateSpra.getListNameOutputFilesSpra(),numberStack, mainNumberStack,countSpra,tempNewOutputNumber);
                }
            count++;


        } catch (Exception e) {
            System.out.print(e);
        }


    }


    public CreateXML(String outputPath, HashMap<String, String> hashMap,ArrayList<String> listNameInputFiles,int outputNumber) {

        this(outputPath, hashMap, null, null, null,listNameInputFiles,outputNumber);

    }





}

