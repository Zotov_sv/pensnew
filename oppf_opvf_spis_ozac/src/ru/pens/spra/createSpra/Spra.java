package ru.pens.spra.createSpra;

import ru.pens.posd.createPosd.PosdHeaderFile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Admin on 07.04.2017.
 */
@XmlRootElement(name = "ФайлПФР")
@XmlAccessorType(XmlAccessType.FIELD)
public class Spra {

    @XmlElement(name = "ИмяФайла")
    private String nameFile;

    @XmlElement(name = "ЗаголовокФайла")
    private SpraHeaderFile headerFile;

    @XmlElement(name = "ПачкаИсходящихДокументов")
    private SpraDocs spraDocs;

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public void setHeaderFile(SpraHeaderFile headerFile) {
        this.headerFile = headerFile;
    }

    public void setSpraDocs(SpraDocs spraDocs) {
        this.spraDocs = spraDocs;
    }


}
