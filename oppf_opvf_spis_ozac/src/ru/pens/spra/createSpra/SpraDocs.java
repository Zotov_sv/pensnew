package ru.pens.spra.createSpra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 07.04.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class SpraDocs {

    @XmlAttribute(name = "ДоставочнаяОрганизация")
    private String bank = "Банк";

    @XmlElement (name = "ВХОДЯЩАЯ_ОПИСЬ")
    private SpraInputInventory spraInputInventory;

    @XmlElement (name = "ИСХОДЯЩАЯ_ОПИСЬ")
    private SpraOutputInventory spraOutputInventory;

    @XmlElement(name = "СПИСОК_РАСХОЖДЕНИЙ")
    private SpraListOfDiscrepancies spraListOfDiscrepancies;

    public void setSpraInputInventory(SpraInputInventory spraInputInventory) {
        this.spraInputInventory = spraInputInventory;
    }

    public void setSpraOutputInventory(SpraOutputInventory spraOutputInventory) {
        this.spraOutputInventory = spraOutputInventory;
    }

    public void setSpraListOfDiscrepancies(SpraListOfDiscrepancies spraListOfDiscrepancies) {
        this.spraListOfDiscrepancies = spraListOfDiscrepancies;
    }
}
