package ru.pens.spra.createSpra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 24.04.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpraHeaderFileTrainingProgramData {

    @XmlElement(name = "НазваниеПрограммы")
    private String nameOfMyProgram;

    @XmlElement(name = "Версия")
    private String version;

    public void setNameOfMyProgram(String nameOfMyProgram) {
        this.nameOfMyProgram = nameOfMyProgram;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
