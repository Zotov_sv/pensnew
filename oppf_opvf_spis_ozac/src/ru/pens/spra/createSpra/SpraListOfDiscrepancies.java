package ru.pens.spra.createSpra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by Admin on 07.04.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpraListOfDiscrepancies {

    @XmlElement (name = "Расхождения")
    private List<SpraListOfDiscrepanciesDifference> spraDifferenceList;

    @XmlElement (name = "Количество")
    private String quantity;

    @XmlElement(name = "ДатаВыдачиДокумента")
    private String DateIssueOfTheDocument;

    public List<SpraListOfDiscrepanciesDifference> getSpraDifferenceList() {
        return spraDifferenceList;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getDateIssueOfTheDocument() {
        return DateIssueOfTheDocument;
    }

    public void setSpraDifferenceList(List<SpraListOfDiscrepanciesDifference> spraDifferenceList) {
        this.spraDifferenceList = spraDifferenceList;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setDateIssueOfTheDocument(String dateIssueOfTheDocument) {
        DateIssueOfTheDocument = dateIssueOfTheDocument;
    }
}
