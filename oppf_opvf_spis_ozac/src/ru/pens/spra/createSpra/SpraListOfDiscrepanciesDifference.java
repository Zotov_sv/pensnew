package ru.pens.spra.createSpra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 07.04.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class SpraListOfDiscrepanciesDifference {

    @XmlElement (name = "НомерВмассиве")
    public int idArray;
    @XmlElement(name = "НомерВыплатногоДела")
    private String numberPaymasterDeal;
    @XmlElement(name = "КодРайона")
    private String areaCode;
    @XmlElement(name = "СтраховойНомер")
    private String insuranceNumber;
    @XmlElement(name = "НомерСчетаПФР")
    private String pfrBill;
    @XmlElement(name = "НомерСчетаБанк")
    private String bankBill;
    @XmlElement(name = "КодРасхождения")
    private String differenceCode;
    @XmlElement(name = "Примечание")
    private String note;

    public int getIdArray() {
        return idArray;
    }

    public String getNumberPaymasterDeal() {
        return numberPaymasterDeal;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public String getPfrBill() {
        return pfrBill;
    }

    public String getBankBill() {
        return bankBill;
    }

    public String getDifferenceCode() {
        return differenceCode;
    }

    public String getNote() {
        return note;
    }

    public void setIdArray(int idArray) {
        this.idArray = idArray;
    }

    public void setNumberPaymasterDeal(String numberPaymasterDeal) {
        this.numberPaymasterDeal = numberPaymasterDeal;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public void setPfrBill(String pfrBill) {
        this.pfrBill = pfrBill;
    }

    public void setBankBill(String bankBill) {
        this.bankBill = bankBill;
    }

    public void setDifferenceCode(String differenceCode) {
        this.differenceCode = differenceCode;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
