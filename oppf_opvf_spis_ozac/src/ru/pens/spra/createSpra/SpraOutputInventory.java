package ru.pens.spra.createSpra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 07.04.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class SpraOutputInventory {

    @XmlElement(name = "СоставительПачки")
    private SpraOutputInventoryCompiledPacks spraCompiliedPacks;

    @XmlElement (name = "СоставДокументов")
    private SpraOutputInventoryTheDocuments spraOutputInventoryTheDocuments;

    @XmlElement (name = "ТерриториальныйОрганПФР")
    private SpraOutputInventoryTerritorialAuthorityPFR spraAuthorityPFR;

    @XmlElement (name = "НомерБанка")
    private String numberBank;

    @XmlElement (name = "ОрганизацияСформировавшаяДокумент")
    private SpraOutputInventoryCompanyFormedDocuments spraCompanyFormedDocuments;

    @XmlElement (name = "ТипМассиваПоручений")
    private String signArraysOrder;

    @XmlElement (name = "Месяц")
    private String month;

    @XmlElement (name = "Год")
    private String year;

    @XmlElement (name = "ИсходящийНомер")
    private String ouputNumber;

    @XmlElement (name = "КоличествоЧастейОтчета")
    private int numberPartsOfTheReports;

    @XmlElement (name = "ДатаФормирования")
    private String dateFormation;

    @XmlElement (name = "Должность")
    private String position;

    @XmlElement (name = "Руководитель")
    private String head;

    public void setSpraCompiliedPacks(SpraOutputInventoryCompiledPacks spraCompiliedPacks) {
        this.spraCompiliedPacks = spraCompiliedPacks;
    }

    public void setSpraOutputInventoryTheDocuments(SpraOutputInventoryTheDocuments spraOutputInventoryTheDocuments) {
        this.spraOutputInventoryTheDocuments = spraOutputInventoryTheDocuments;
    }

    public void setSpraAuthorityPFR(SpraOutputInventoryTerritorialAuthorityPFR spraAuthorityPFR) {
        this.spraAuthorityPFR = spraAuthorityPFR;
    }

    public void setNumberBank(String numberBank) {
        this.numberBank = numberBank;
    }

    public void setSpraCompanyFormedDocuments(SpraOutputInventoryCompanyFormedDocuments spraCompanyFormedDocuments) {
        this.spraCompanyFormedDocuments = spraCompanyFormedDocuments;
    }

    public void setSignArraysOrder(String signArraysOrder) {
        this.signArraysOrder = signArraysOrder;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setOuputNumber(String ouputNumber) {
        this.ouputNumber = ouputNumber;
    }

    public void setDateFormation(String dateFormation) {
        this.dateFormation = dateFormation;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public void setNumberPartsOfTheReports(int numberPartsOfTheReports) {
        this.numberPartsOfTheReports = numberPartsOfTheReports;
    }
}
