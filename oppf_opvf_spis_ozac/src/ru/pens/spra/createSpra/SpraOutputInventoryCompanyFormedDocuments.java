package ru.pens.spra.createSpra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 07.04.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpraOutputInventoryCompanyFormedDocuments {

    @XmlElement(name = "НалоговыйНомер")
    private SpraOutputInventoryCompanyFormedDocumentsTaxNumber spraPacksTaxNumber;

    @XmlElement (name = "НаименованиеОрганизации")
    private String nameOfCompany;

    @XmlElement(name = "РегистрационныйНомер")
    private String  packsRegistrationNumber;

    @XmlElement(name = "Подразделение")
    private SpraOutputInventoryCompanyFormedDocumentsUnit spraCompiledPacksUnit;


    public void setSpraPacksTaxNumber(SpraOutputInventoryCompanyFormedDocumentsTaxNumber spraPacksTaxNumber) {
        this.spraPacksTaxNumber = spraPacksTaxNumber;
    }

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }

    public void setPacksRegistrationNumber(String packsRegistrationNumber) {
        this.packsRegistrationNumber = packsRegistrationNumber;
    }

    public void setSpraCompiledPacksUnit(SpraOutputInventoryCompanyFormedDocumentsUnit spraCompiledPacksUnit) {
        this.spraCompiledPacksUnit = spraCompiledPacksUnit;
    }
}
