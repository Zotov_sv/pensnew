package ru.pens.spra.createSpra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 07.04.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class SpraOutputInventoryCompanyFormedDocumentsUnit {

    @XmlElement(name = "НаименованиеПодразделения")
    private String nameUnit;

    @XmlElement (name = "НомерПодразделения")
    private String numberUnit;

    public void setNameUnit(String nameUnit) {
        this.nameUnit = nameUnit;
    }

    public void setNumberUnit(String numberUnit) {
        this.numberUnit = numberUnit;
    }

}
