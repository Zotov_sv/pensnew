package ru.pens.spra.createSpra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 07.04.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SpraOutputInventoryCompiledPacksTaxNumber {

    @XmlElement(name = "ИНН")
    private String inn;

    @XmlElement (name = "КПП")
    private String kpp;

    public void setInn(String inn) {
        this.inn = inn;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }
}

