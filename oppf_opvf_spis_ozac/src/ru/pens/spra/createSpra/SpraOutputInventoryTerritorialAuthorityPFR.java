package ru.pens.spra.createSpra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 07.04.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class SpraOutputInventoryTerritorialAuthorityPFR {

    @XmlElement (name = "НалоговыйНомер")
    private SpraOutputInventoryTerritorialAuthorityPFRTaxNumber spraPfrTaxNumber;

    @XmlElement (name = "НаименованиеОрганизации")
    private String nameOfCompany;

    @XmlElement (name = "РегистрационныйНомер")
    private String registrationNumber;

    public void setSpraPfrTaxNumber(SpraOutputInventoryTerritorialAuthorityPFRTaxNumber spraPfrTaxNumber) {
        this.spraPfrTaxNumber = spraPfrTaxNumber;
    }

    public void setNameOfCompany(String nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
