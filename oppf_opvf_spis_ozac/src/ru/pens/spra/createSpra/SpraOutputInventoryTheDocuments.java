package ru.pens.spra.createSpra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 07.04.2017.
 */


@XmlAccessorType(XmlAccessType.FIELD)
public class SpraOutputInventoryTheDocuments {

    @XmlElement(name = "Количество")
    private int quantity;

    @XmlElement (name = "НаличиеДокументов")
    private SpraOutputInventoryTheDocumentsAvailabilityOfDocuments spraAvailabilityOfDocuments;

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setSpraAvailabilityOfDocuments(SpraOutputInventoryTheDocumentsAvailabilityOfDocuments spraAvailabilityOfDocuments) {
        this.spraAvailabilityOfDocuments = spraAvailabilityOfDocuments;
    }
}
