package ru.pens.spra.createSpra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Admin on 07.04.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class SpraOutputInventoryTheDocumentsAvailabilityOfDocuments {

    @XmlElement(name = "ТипДокумента")
    private String signDocument;

    @XmlElement (name = "Количество")
    private int quantity;

    public void setSignDocument(String signDocument) {
        this.signDocument = signDocument;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
