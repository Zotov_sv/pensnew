package ru.pens.spra.withFiles;

import ru.pens.global.counter.AllCounter;
import ru.pens.oppf_opvf.dataProcessing.ParsOppf;
import ru.pens.spis_ozac.dataProcessing.Person;
import ru.pens.spra.createSpra.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Admin on 11.04.2017.
 */
public class CreateSpra {


    //private static int count = 0;

    /* public CreateSpra(LinkedHashMap<String, Person> map, String nameFileOzac, HashMap<String,
             String> hashMap, String outputPath,
                       ArrayList<String> listNameInputFiles,
                       ArrayList<String> listNameOutputFiles,ArrayList<String> listNameOutputFilesSpra){*/
    //public CreateSpra(LinkedHashMap<String, Person> map,ArrayList<String> listNameOutputFiles){

    private static ArrayList<String> listNameOutputFilesSpra = new ArrayList<>();
    Spra spra = new Spra();
    private CreateSpra() {}

    private static class HelpCreateSpra {
        private static CreateSpra createSpra = new CreateSpra();
    }

    public static CreateSpra createSpra() {
        return HelpCreateSpra.createSpra;
    }

    public void createSpraXml(LinkedHashMap<String, Person> map, String nameFileOzac, HashMap<String,
            String> hashMap, String outputPath,
                              ArrayList<String> listNameInputFiles,
                              ArrayList<String> listNameOutputFiles,int tmpCountSpra,int count,int outputNumber){

        SpraDocs spraDocs = new SpraDocs();



        /*Место для заголовка*/
        SpraHeaderFile headerFile = new SpraHeaderFile();
        headerFile.setVersionFormat("07.00");
        headerFile.setTypeFile("ВНЕШНИЙ");
        SpraHeaderFileTrainingProgramData trainingProgramData = new SpraHeaderFileTrainingProgramData();
        trainingProgramData.setNameOfMyProgram("МосуралАйтиПродакшн");
        trainingProgramData.setVersion("ВЕРСИЯ1.12");
        headerFile.setProgramData(trainingProgramData);
        headerFile.setDataSource("ДОСТАВЩИК");
        spra.setHeaderFile(headerFile);


        /**********************/

        /*Блок для Входящей описи*/
        //SpraInputInventory spraInputInventory = new SpraInputInventory();
        /********************/
        /* Блок для Исходящей описи*/
        SpraOutputInventory spraOutputInventory = new SpraOutputInventory();
            /*Блок для Составитель пачки*/
            SpraOutputInventoryCompiledPacks compiledPacks = new SpraOutputInventoryCompiledPacks();
                SpraOutputInventoryCompiledPacksTaxNumber compiledPacksTaxNumber = new SpraOutputInventoryCompiledPacksTaxNumber();
                compiledPacksTaxNumber.setInn(hashMap.get("INN"));
                compiledPacksTaxNumber.setKpp(hashMap.get("KPP"));
            compiledPacks.setSpraPacksTaxNumber(compiledPacksTaxNumber);
            compiledPacks.setNameOfCompany(hashMap.get("NameCompany"));
            compiledPacks.setPacksRegistrationNumber(hashMap.get("Registration Number"));
                SpraOutputInventoryCompiledPacksUnit compiledPacksUnit = new SpraOutputInventoryCompiledPacksUnit();
                compiledPacksUnit.setNameUnit(hashMap.get("Unit Company"));
                compiledPacksUnit.setNumberUnit(hashMap.get("Unit RegistrationNumber"));
            compiledPacks.setSpraCompiledPacksUnit(compiledPacksUnit);
        /*******************************************************/
            /*Блок для Состав документов*/
            SpraOutputInventoryTheDocuments inventoryTheDocuments = new SpraOutputInventoryTheDocuments();
            inventoryTheDocuments.setQuantity(listNameOutputFilesSpra.size()+1);
                SpraOutputInventoryTheDocumentsAvailabilityOfDocuments availabilityOfDocuments = new SpraOutputInventoryTheDocumentsAvailabilityOfDocuments();
                int tmpQuantityListDiscperance = 0;
                for (Map.Entry x : map.entrySet()) {
                    Person person = (Person) x.getValue();
                    if (person.enrollmentCode.equals("32")) {
                        tmpQuantityListDiscperance++;
                    }
                }
                availabilityOfDocuments.setQuantity(tmpQuantityListDiscperance);
                availabilityOfDocuments.setSignDocument("СПИСОК_РАСХОЖДЕНИЙ");
            inventoryTheDocuments.setSpraAvailabilityOfDocuments(availabilityOfDocuments);
        /*******************************************************/
            /*Блок для Территориальный орган ПФР*/
            SpraOutputInventoryTerritorialAuthorityPFR authorityPFR = new SpraOutputInventoryTerritorialAuthorityPFR();
                SpraOutputInventoryTerritorialAuthorityPFRTaxNumber authorityPFRTaxNumber = new SpraOutputInventoryTerritorialAuthorityPFRTaxNumber();
                authorityPFRTaxNumber.setInn(hashMap.get("AuthorityPfrInn"));
                authorityPFRTaxNumber.setKpp(hashMap.get("AuthorityPfrKpp"));
            authorityPFR.setSpraPfrTaxNumber(authorityPFRTaxNumber);
            authorityPFR.setNameOfCompany(hashMap.get("AuthorityPfrNameCompany"));
            authorityPFR.setRegistrationNumber(hashMap.get("AuthorityPfrRegistrationNumber"));
        /*******************************************************/
            /*Блок для Организация сформировавшая документ*/
            SpraOutputInventoryCompanyFormedDocuments formedDocuments = new SpraOutputInventoryCompanyFormedDocuments();
                SpraOutputInventoryCompanyFormedDocumentsTaxNumber formedDocumentsTaxNumber = new SpraOutputInventoryCompanyFormedDocumentsTaxNumber();
                formedDocumentsTaxNumber.setInn(hashMap.get("INN"));
                formedDocumentsTaxNumber.setKpp(hashMap.get("KPP"));
            formedDocuments.setSpraPacksTaxNumber(formedDocumentsTaxNumber);
            formedDocuments.setNameOfCompany(hashMap.get("NameCompany"));
            formedDocuments.setPacksRegistrationNumber(hashMap.get("Registration Number"));
                SpraOutputInventoryCompanyFormedDocumentsUnit formedDocumentsUnit = new SpraOutputInventoryCompanyFormedDocumentsUnit();
                formedDocumentsUnit.setNameUnit(hashMap.get("Unit Company"));
                formedDocumentsUnit.setNumberUnit(hashMap.get("Unit RegistrationNumber"));
            formedDocuments.setSpraCompiledPacksUnit(formedDocumentsUnit);
        /*******************************************************/

        /*Блок для Исходящий Номер*/
            int tempNewOutputNumber = outputNumber + 1;
                String tmpNewOutputNumber = "";
            if (tempNewOutputNumber > 0 & tempNewOutputNumber <= 9) {
                tmpNewOutputNumber = "000000000" + tempNewOutputNumber;
            } else if (tempNewOutputNumber > 9 & tempNewOutputNumber <= 99) {
                tmpNewOutputNumber = "00000000" + tempNewOutputNumber;
            } else if (tempNewOutputNumber > 99 & tempNewOutputNumber <= 999) {
                tmpNewOutputNumber = "0000000" + tempNewOutputNumber;
            } else if (tempNewOutputNumber > 999 & tempNewOutputNumber <= 9999) {
                tmpNewOutputNumber = "000000" + tempNewOutputNumber;
            } else if (tempNewOutputNumber > 9999 & tempNewOutputNumber <= 99999) {
                tmpNewOutputNumber = "00000" + tempNewOutputNumber;
            } else if (tempNewOutputNumber > 99999 & tempNewOutputNumber <= 999999) {
                tmpNewOutputNumber = "0000" + tempNewOutputNumber;
            } else if (tempNewOutputNumber > 999999 & tempNewOutputNumber <= 9999999) {
                tmpNewOutputNumber = "000" + tempNewOutputNumber;
            } else if (tempNewOutputNumber > 9999999 & tempNewOutputNumber <= 99999999) {
                tmpNewOutputNumber = "00" + tempNewOutputNumber;
            } else if (tempNewOutputNumber > 99999999 & tempNewOutputNumber <= 999999999) {
                tmpNewOutputNumber = "0" + tempNewOutputNumber;
            }
        /*******************************************************/
        spraOutputInventory.setSpraCompiliedPacks(compiledPacks);
        spraOutputInventory.setSpraOutputInventoryTheDocuments(inventoryTheDocuments);
        spraOutputInventory.setSpraAuthorityPFR(authorityPFR);
        spraOutputInventory.setNumberBank(hashMap.get("NumberBank"));
        spraOutputInventory.setSpraCompanyFormedDocuments(formedDocuments);
        spraOutputInventory.setSignArraysOrder(hashMap.get("SignArraysOrder"));
        spraOutputInventory.setMonth(hashMap.get("Month"));
        spraOutputInventory.setYear(hashMap.get("Year"));
        spraOutputInventory.setOuputNumber(tmpNewOutputNumber);
        spraOutputInventory.setNumberPartsOfTheReports(tmpQuantityListDiscperance);
        spraOutputInventory.setDateFormation(hashMap.get("DateFormation"));
        spraOutputInventory.setPosition(hashMap.get("Position"));
        spraOutputInventory.setHead(hashMap.get("Head"));
        /********************/


        /*Блок для Списка расхождений*/
        SpraListOfDiscrepancies spraListOfDiscrepancies = new SpraListOfDiscrepancies();
            ArrayList<SpraListOfDiscrepanciesDifference> listDefferenceMap = new ArrayList<>();
            int quantityListDiscperance = 0;
            for (Map.Entry x : map.entrySet()){
                Person person = (Person) x.getValue();
                if (person.enrollmentCode.equals("32")){
                    SpraListOfDiscrepanciesDifference listOfDiscrepanciesDifference = new SpraListOfDiscrepanciesDifference();
                    listOfDiscrepanciesDifference.setIdArray(person.idArray);
                    listOfDiscrepanciesDifference.setNumberPaymasterDeal(person.numberPaymasterDeal);
                    listOfDiscrepanciesDifference.setAreaCode(person.areaCode);
                    listOfDiscrepanciesDifference.setInsuranceNumber(person.insuranceNumber);
                    listOfDiscrepanciesDifference.setPfrBill(person.bill);
                    listOfDiscrepanciesDifference.setBankBill(person.billBank);
                    listOfDiscrepanciesDifference.setDifferenceCode(person.enrollmentCode);
                    listOfDiscrepanciesDifference.setNote(person.note);
                    listDefferenceMap.add(listOfDiscrepanciesDifference);
                    quantityListDiscperance++;
                }

            }
            spraListOfDiscrepancies.setSpraDifferenceList(listDefferenceMap);
            spraListOfDiscrepancies.setQuantity(String.valueOf(quantityListDiscperance));
            spraListOfDiscrepancies.setDateIssueOfTheDocument(hashMap.get("DateFormation"));
        /********************/
        //spraDocs.setSpraInputInventory(spraInputInventory);
        spraDocs.setSpraOutputInventory(spraOutputInventory);
        spraDocs.setSpraListOfDiscrepancies(spraListOfDiscrepancies);




        //int newOutputNumber = Integer.valueOf((hashMap.get("OutputNumber")));


        int numberStack = ParsOppf.getOppf().getOppfInputDoc().getInputInventory().getNumberStack();
        int mainNumberStack = ParsOppf.getOppf().getOppfInputDoc().getInputInventory().getInputInventoryTwinPack().getMain();
        String tmpMainNumberStack;
        String tmpNumberStack;
        if (numberStack > 0 & numberStack <= 9) {
            tmpNumberStack = "00" + numberStack;
        } else if (numberStack > 9 & numberStack <= 99) {
            tmpNumberStack = "0" + numberStack;
        } else {
            tmpNumberStack = String.valueOf(numberStack);
        }

        if (mainNumberStack > 0 & mainNumberStack <= 9) {
            tmpMainNumberStack = "0000" + mainNumberStack;
        } else if (mainNumberStack > 9 & mainNumberStack <= 99) {
            tmpMainNumberStack = "000" + mainNumberStack;
        } else if (mainNumberStack > 99 & mainNumberStack <= 999) {
            tmpMainNumberStack = "00" + mainNumberStack;
        } else if (mainNumberStack > 999 & mainNumberStack <= 9999) {
            tmpMainNumberStack = "0" + mainNumberStack;
        } else {
            tmpMainNumberStack = String.valueOf(mainNumberStack);
        }

        String name = "OUT-700-Y-" + hashMap.get("Year") + "-ORG-" + hashMap.get("AuthorityPfrRegistrationNumber")
                + "-DIS-038-DCK-" + tmpMainNumberStack + "-" + tmpNumberStack + "-DOC-SPRA-FSB-"
                + hashMap.get("NumberBank") + "-OUTNMB-" + tmpNewOutputNumber + ".XML";
        spra.setNameFile(name);
        spra.setSpraDocs(spraDocs);





        try {
            File file = new File(outputPath + "\\" + name);
            JAXBContext jc = JAXBContext.newInstance(Spra.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(spra, file);
            //marshaller.marshal(spra, System.out);
            listNameOutputFilesSpra.add(name);

            AllCounter.allCounter().recordCount(tempNewOutputNumber);
        } catch (Exception e) {
            System.out.print(e);
        }

    }

    public static ArrayList<String> getListNameOutputFilesSpra() {
        return listNameOutputFilesSpra;
    }


}
